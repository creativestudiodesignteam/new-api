import {AppError} from "src/shared/error/AppError";

export class EggHasAlreadyBeenHatched extends AppError {
  constructor() {
    super("Ovo já foi chocado", 400);
  }
}
