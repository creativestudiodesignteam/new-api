export type AllowedBirdsRelation =
  | "category"
  | "breeds"
  | "color"
  | "status"
  | "cage"
  | "parents";
