import {Injectable} from "@nestjs/common";
import {CategoriesService} from "src/categories/categories.service";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {AppError} from "src/shared/error/AppError";
import {DeepPartial} from "typeorm";
import {BreedsRepository} from "./breed.repository";
import {BreedsColorsService} from "./colors/colors.service";
import {ColorsObject} from "./colors/interface/ColorsObject";
import {BreedDTO} from "./dto/breed.dto";
import {BreedsCreateDto} from "./dto/create.dto";
import {InCreateBreed} from "./dto/createIn.dto";
import {BreedEditDTO} from "./dto/edit.dto";
import {BreedNotExists} from "./exception/breedAlreadyExists";
import {BreedTypeService} from "./type/type.service";

@Injectable()
export class BreedService {
  constructor(
    private breedsRepository: BreedsRepository,
    private breedsTypesService: BreedTypeService,
    private categoriesService: CategoriesService,
    private breedsColorsService: BreedsColorsService,
  ) {}

  async findAll(queryParams: QueryParams) {
    return await this.breedsRepository.list(queryParams);
  }

  async findByCategory(id_category, queryParams) {
    const checkCategory = await this.categoriesService.findById(id_category);

    const findBreedAll = await this.breedsRepository.findByWithRelation(
      {
        category: checkCategory,
      },
      queryParams,
      ["category", "breed_type"],
    );

    return findBreedAll;
  }

  async findById(id: number) {
    const breed = await this.breedsRepository.findById(id);

    if (!breed) throw new BreedNotExists();

    const colorsToBreed = await this.breedsColorsService.findByBreeds(breed);

    const breedInReturn = new BreedDTO(
      breed.id,
      breed.name,
      breed.category,
      breed.breed_type,
      colorsToBreed,
    );

    return breedInReturn;
  }

  async findByIdOnlyBreed(id: number) {
    const breed = await this.breedsRepository.findById(id);

    if (!breed) throw new BreedNotExists();

    const breedInReturn = new BreedDTO(
      breed.id,
      breed.name,
      breed.category,
      breed.breed_type,
    );

    return breedInReturn;
  }

  async create(breedToBeCreated: InCreateBreed): Promise<BreedDTO> {
    const checkBreedType = await this.breedsTypesService.findById(
      breedToBeCreated.breed_type,
    );

    const checkCategories = await this.categoriesService.findById(
      breedToBeCreated.category,
    );

    let checkColors;
    if (breedToBeCreated.colors && breedToBeCreated.colors.length > 0) {
      checkColors = await this.breedsColorsService.findById(
        breedToBeCreated.colors,
      );
    }

    const breedsCreate = new BreedsCreateDto();
    breedsCreate.name = breedToBeCreated.name;
    breedsCreate.breed_type = checkBreedType;
    breedsCreate.category = checkCategories;
    breedsCreate.colors = checkColors;

    await this.breedsRepository.store(breedsCreate);

    if (breedToBeCreated.colors && breedToBeCreated.colors.length > 0) {
      await this.breedsColorsService.create(
        breedsCreate,
        breedToBeCreated.colors,
      );
    }

    return breedsCreate;
  }

  async update(id: number, breedToEdit: DeepPartial<BreedEditDTO>) {
    const checkBreedType = await this.breedsTypesService.findById(
      breedToEdit.breed_type,
    );

    const checkCategories = await this.categoriesService.findById(
      breedToEdit.category,
    );

    const breedsFinded = await this.breedsRepository.findById(id);

    if (!breedsFinded) throw new BreedNotExists();

    const breeds = this.breedsRepository.create({
      id,
      name: breedToEdit.name,
      breed_type: checkBreedType,
      category: checkCategories,
    });

    await this.breedsRepository.updateBreeds(breeds);

    return breeds;
  }

  async delete(id: number) {
    const breed = await this.breedsRepository.findWithRelation(id, [
      "breed_type",
      "category",
    ]);

    if (!breed) throw new BreedNotExists();

    this.breedsRepository.destroy(breed);
  }

  async addRelation(
    breed: number,
    colorsToBeRelation: ColorsObject[],
  ): Promise<any> {
    const checkBreedExists = await this.breedsRepository.findById(breed);

    await Promise.all(
      colorsToBeRelation.map(async result => {
        const findColorsByColorAndBreed = await this.breedsColorsService.findByColorAndBreeds(
          result.id,
          breed,
        );

        if (findColorsByColorAndBreed.length > 0)
          throw new AppError(
            "Algumas cores selecionadas, já estão cadastrada a esta raça, tente novamente",
          );

        return findColorsByColorAndBreed;
      }),
    );

    if (!checkBreedExists) throw new BreedNotExists();

    await this.breedsColorsService.findById(colorsToBeRelation);

    const createdRelation = await this.breedsColorsService.create(
      checkBreedExists.id,
      colorsToBeRelation,
    );

    return createdRelation;
  }
}
