import {IsString} from "class-validator";
export class TransformToBirdDto {
  id: number;
  @IsString()
  name: string;
  birthdate: Date;
  @IsString()
  gender: "Macho" | "Fêmea";
  @IsString()
  registry: string;
  category: number;
  breeds: number;
  color: number;
  status: number;
  account: number;
  cage: number;
}
