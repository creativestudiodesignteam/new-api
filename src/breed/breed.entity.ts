import {Categories} from "src/categories/categories.entity";
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import {BreedsTypes} from "./type/type.entity";
import {BreedsColors} from "./colors/colors.entity";
import { Birds } from "src/bird/bird.entity";

@Entity({name: "breeds"})
export class Breeds {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(
    () => BreedsTypes,
    breedsTypes => breedsTypes.breeds,
  )
  @JoinColumn({name: "breed_type"})
  breed_type: BreedsTypes;

  @ManyToOne(
    () => Categories,
    categories => categories.breeds,
  )
  @JoinColumn({name: "category"})
  category: Categories;

  @OneToMany(
    () => BreedsColors,
    breedsColors => breedsColors,
  )
  breedsColors: BreedsColors;

  @OneToMany(
    () => Birds,
    birds => birds.breeds,
  )
  birds: Birds;
}
