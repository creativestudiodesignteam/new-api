import {DeepPartial, EntityRepository, Repository} from "typeorm";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {BreedsTypes} from "./type.entity";

@EntityRepository(BreedsTypes)
export class BreedTypeRepository extends Repository<BreedsTypes> {
  async list(queryParams: QueryParams): Promise<any> {
    const breedsType = await this.find({
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      order: {name: "ASC"},
    });

    return breedsType;
  }

  async findById(id: number): Promise<BreedsTypes> {
    return await this.findOne(id);
  }

  async findBy(paramToSearch) {
    return await this.findOne({where: paramToSearch});
  }

  async store(breedsTypeToBeCreated): Promise<any> {
    return await this.insert(breedsTypeToBeCreated);
  }

  async updateBreedsType(breedsType: DeepPartial<BreedsTypes>) {
    return await this.update(breedsType.id, breedsType);
  }

  async destroy(breedsType): Promise<void> {
    await this.remove(breedsType);
  }
}
