import {AppError} from "src/shared/error/AppError";

export class AccountNotExists extends AppError {
  constructor() {
    super("Conta inexisteste", 404);
  }
}
