import {Injectable} from "@nestjs/common";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {StatusCreateDto} from "./dto/create.dto";
import {StatusDTO} from "./dto/status.dto";
import {StatusNotExists} from "./exceptions/statusNotExists";
import {Status} from "./status.entity";
import {StatusRepository} from "./status.repository";

@Injectable()
export class StatusService {
  constructor(private statusRepository: StatusRepository) {}

  async findAll(queryParams: QueryParams) {
    return await this.statusRepository.list(queryParams);
  }

  async findById(id: number): Promise<StatusDTO> {
    const StatusFinded = await this.statusRepository.findById(id);

    if (!StatusFinded) throw new StatusNotExists();

    return StatusFinded;
  }

  async create(statusToBeCreated: StatusCreateDto): Promise<StatusCreateDto> {
    const status = new Status();
    status.name = statusToBeCreated.name;

    await this.statusRepository.store(status);

    return status;
  }

  async delete(id: number) {
    const status = await this.statusRepository.findById(id);

    if (!status) throw new StatusNotExists();
    this.statusRepository.destroy(status);
  }
}
