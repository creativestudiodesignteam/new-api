import {AppError} from "src/shared/error/AppError";

export class BirdsNotExists extends AppError {
  constructor() {
    super("Pássaro inexistente", 404);
  }
}
