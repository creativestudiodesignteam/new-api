import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
  ValidationPipe,
} from "@nestjs/common";
import {JwtAuthGuard} from "src/auth/jwt/jwt-auth.guard";
import {CustomSuccessResponse} from "src/shared/dto/customResponse.dto";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {BreedsTypesCreateDto} from "./dto/create.dto";
import {BreedsTypeEditDTO} from "./dto/edit.dto";
import {TypesListDto} from "./dto/list.dto";
import {BreedsTypesDTO} from "./dto/types.dto";
import {BreedTypeService} from "./type.service";

@Controller("breeds/types")
export class TypeController {
  constructor(private typesService: BreedTypeService) {}

  @UseGuards(JwtAuthGuard)
  @Get("read")
  async findAll(
    @Query(new ValidationPipe()) queryParams: QueryParams,
  ): Promise<any> {
    const breedsTypesFindAll = await this.typesService.findAll(queryParams);

    const paginatedList = new TypesListDto(
      breedsTypesFindAll,
      queryParams.page ?? 1,
      queryParams.perPage ?? 10,
    );

    return paginatedList;
  }

  @UseGuards(JwtAuthGuard)
  @Get("read/:id")
  async findByUser(@Param() param): Promise<BreedsTypesDTO> {
    const BreedsTypes = await this.typesService.findById(param.id);
    return BreedsTypes;
  }

  @Post()
  async create(
    @Body(new ValidationPipe()) typeToBeCreate: BreedsTypesCreateDto,
  ): Promise<BreedsTypesDTO> {
    return await this.typesService.create(typeToBeCreate);
  }

  @Put(":id")
  async update(
    @Param() param,
    @Body(new ValidationPipe())
    breedsTypeToEdit: BreedsTypeEditDTO,
  ) {
    return await this.typesService.update(param.id, breedsTypeToEdit);
  }

  @Delete(":id")
  @HttpCode(200)
  async delete(@Param() param): Promise<CustomSuccessResponse> {
    await this.typesService.delete(param.id);

    return new CustomSuccessResponse("Tipo de raça excluida");
  }
}
