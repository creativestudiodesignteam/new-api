import {IsDateString, IsNumber, IsString} from "class-validator";
export class ImageWithLink {
  @IsNumber()
  id: number;
  @IsString()
  name: string;
  @IsString()
  path: string;
  @IsString()
  url: string;
}
