import {AppError} from "src/shared/error/AppError";

export class CpfInválid extends AppError {
  constructor() {
    super("CPF inválido, verifique se está correto", 400);
  }
}
