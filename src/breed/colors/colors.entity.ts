import {Colors} from "src/colors/colors.entity";
import {ColorsDTO} from "src/colors/dto/colors.dto";
import {Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Breeds} from "../breed.entity";

@Entity({name: "breeds_colors"})
export class BreedsColors {
  @PrimaryGeneratedColumn()
  id: number;
  @ManyToOne(
    () => Breeds,
    breeds => breeds.breedsColors,
  )
  @JoinColumn({name: "breed"})
  breed: Breeds;

  @ManyToOne(
    () => Colors,
    colors => colors.breedsColors,
  )
  @JoinColumn({name: "breed_color"})
  breed_color: ColorsDTO;
}
