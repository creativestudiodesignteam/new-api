import {IsString} from "class-validator";

export class BreedsTypeEditDTO {
  @IsString()
  name: string;
}
