import {Account} from "src/account/account.entity";
import {Breeds} from "src/breed/breed.entity";
import {Categories} from "src/categories/categories.entity";
import {Colors} from "src/colors/colors.entity";
import {Status} from "src/status/status.entity";
import {Cage} from "src/cage/cage.entity";

import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import {Gallery} from "./gallery/gallery.entity";
import { Parent } from "./parent/parent.entity";

@Entity({name: "birds"})
export class Birds {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  birthdate: Date;

  @Column()
  rightRing: string;

  @Column()
  leftRing: string;

  @Column()
  gender: string;

  @Column()
  registry: string;

  @ManyToOne(
    () => Account,
    accounts => accounts.birds,
  )
  @JoinColumn({name: "account"})
  account: Account;

  @ManyToOne(
    () => Categories,
    category => category.birds,
  )
  @JoinColumn({name: "category"})
  category: Categories;

  @ManyToOne(
    () => Breeds,
    breeds => breeds.birds,
  )
  @JoinColumn({name: "breed"})
  breeds: Breeds;

  @ManyToOne(
    () => Colors,
    colors => colors.birds,
  )
  @JoinColumn({name: "color"})
  color: Colors;

  @ManyToOne(
    () => Cage,
    cages => cages.bird,
  )
  @JoinColumn({name: "cages"})
  cage: Cage;

  @ManyToOne(
    () => Status,
    status => status.birds,
  )
  @JoinColumn({name: "status"})
  status: Status;

  @ManyToOne(
    () => Parent,
    parent => parent.birds,
  )
  @JoinColumn({name: "parents"})
  parents: Parent;

  @OneToMany(
    () => Gallery,
    gallery => gallery.bird,
  )
  gallery: Gallery;

  
  @OneToMany(
    () => Parent,
    parent => parent.mother,
  )
  mother: Parent;

  @OneToMany(
    () => Parent,
    parent => parent.father,
  )
  father: Parent;
}
