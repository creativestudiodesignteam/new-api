import {Module} from "@nestjs/common";
import {AuthService} from "./auth.service";
import {AuthController} from "./auth.controller";
import {AccountService} from "src/account/account.service";
import {AccountModule} from "src/account/account.module";
import {SharedModule} from "src/shared/shared.module";
import {PassportModule} from "@nestjs/passport";
import {JwtModule} from "@nestjs/jwt";
import {jwtConstants} from "./jwt/constants";
import {JwtStrategy} from "./jwt/jwt.strategy";
import {LocalStrategy} from "./jwt/local.strategy";

@Module({
  imports: [
    AccountModule,
    SharedModule,
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {expiresIn: "7d"},
    }),
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
