import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
  ValidationPipe,
} from "@nestjs/common";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {BreedService} from "./breed.service";
import {BreedDTO} from "./dto/breed.dto";
import {BreedListDto} from "./dto/list.dto";
import {InCreateBreed} from "./dto/createIn.dto";
import {BreedEditDTO} from "./dto/edit.dto";
import {CustomSuccessResponse} from "src/shared/dto/customResponse.dto";
import {BreedsColorsService} from "./colors/colors.service";
import {ColorsObject} from "./colors/interface/ColorsObject";
import {JwtAuthGuard} from "src/auth/jwt/jwt-auth.guard";

@Controller("breed")
export class BreedController {
  constructor(
    private breedService: BreedService,
    private breedsColorsService: BreedsColorsService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get("category/breed/:id")
  async findByCategory(
    @Query(new ValidationPipe()) queryParams: QueryParams,
    @Param() param,
  ): Promise<BreedListDto> {
    const categoriesFindAll = await this.breedService.findByCategory(
      param.id,
      queryParams,
    );

    const paginatedList = new BreedListDto(
      categoriesFindAll,
      queryParams.page ?? 1,
      queryParams.perPage ?? 10,
    );

    return paginatedList;
  }

  @UseGuards(JwtAuthGuard)
  @Get("read")
  async findAll(
    @Query(new ValidationPipe()) queryParams: QueryParams,
  ): Promise<BreedListDto> {
    const CategoriesFindAll = await this.breedService.findAll(queryParams);

    const paginatedList = new BreedListDto(
      CategoriesFindAll,
      queryParams.page ?? 1,
      queryParams.perPage ?? 10,
    );

    return paginatedList;
  }

  @UseGuards(JwtAuthGuard)
  @Get("read/:id")
  async findById(@Param() param) {
    const breed = await this.breedService.findById(param.id);
    return breed;
  }

  @Post()
  async create(
    @Body(new ValidationPipe()) breed: InCreateBreed,
  ): Promise<BreedDTO> {
    const createdBreed = await this.breedService.create(breed);

    return new BreedDTO(
      createdBreed.id,
      createdBreed.name,
      createdBreed.breed_type,
      createdBreed.category,
      createdBreed.colors,
    );
  }

  @Put("edit/:id")
  async update(
    @Param() param,
    @Body(new ValidationPipe())
    breedsEdit: BreedEditDTO,
  ): Promise<any> {
    const updateBreeds = this.breedService.update(param.id, breedsEdit);

    return updateBreeds;
  }

  @Delete(":id")
  @HttpCode(200)
  async delete(@Param() param): Promise<CustomSuccessResponse> {
    await this.breedService.delete(param.id);

    return new CustomSuccessResponse("Raça excluida com sucesso");
  }

  @Post("/color/add/:id")
  async addColorInBreed(
    @Param() param,
    @Body(new ValidationPipe()) colors: ColorsObject[],
  ): Promise<any> {
    const createdBreed = await this.breedService.addRelation(param.id, colors);

    return createdBreed;
  }
}
