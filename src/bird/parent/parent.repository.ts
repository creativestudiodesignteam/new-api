import {DeepPartial, EntityRepository, Repository} from "typeorm";

import {QueryParams} from "src/shared/dto/queryParams.dto";
import {Parent} from "./parent.entity";
import {AllowedParentsRelation} from "./interface/AllowedParentsRelation";

@EntityRepository(Parent)
export class ParentsRepository extends Repository<Parent> {
  async list(queryParams: QueryParams): Promise<Parent[]> {
    const parents = await this.find({
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      order: {id: "ASC"},
    });

    return parents;
  }

  async findById(id: number): Promise<any> {
    return await this.findOne(id);
  }

  async findByWithRelation(paramToSearch, relation: AllowedParentsRelation[]) {
    return this.findOne({where: paramToSearch, relations: relation});
  }

  async findBy(paramToSearch) {
    return await this.findOne({where: paramToSearch});
  }

  async store(parentsToBeCreated: any): Promise<any> {
    return await this.insert(parentsToBeCreated);
  }

  async updateParents(parents: DeepPartial<any>) {
    return await this.update(parents.id, parents);
  }

  async destroy(parents: any): Promise<void> {
    await this.remove(parents);
  }
}
