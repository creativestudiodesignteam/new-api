import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("breeds_colors", table => {
    table.increments("id").primary();
    table
      .integer("breed_color")
      .references("id")
      .inTable("colors")
      .notNullable();
    table
      .integer("breed")
      .references("id")
      .inTable("breeds")
      .notNullable();
    table.timestamps(undefined, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("breeds_colors");
}
