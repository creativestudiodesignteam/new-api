import {ValidationPipe} from "../shared/pipes/validation.pipe";
import {
  Controller,
  Post,
  UseGuards,
  Request,
  Body,
  Get,
  Query,
  Param,
  Put,
  Delete,
  HttpCode,
} from "@nestjs/common";
import {JwtAuthGuard} from "src/auth/jwt/jwt-auth.guard";
import {BirdService} from "./bird.service";
import {BirdsCreateDtoIn} from "./dto/create-in.dto";
import {BirdsCreateDtoOut} from "./dto/create-out.dto";
import {BirdsListDto} from "./dto/list.dto";
import {BirdsToListDTO} from "./dto/birdsToList.dto";
import {QueryParamsWithFilter} from "./dto/queryParamsWithFilter.dto";
import {CustomSuccessResponse} from "src/shared/dto/customResponse.dto";

@Controller("birds")
export class BirdController {
  constructor(private birdService: BirdService) {}

  @UseGuards(JwtAuthGuard)
  @Get("read")
  async findAll(
    @Request() req,
    @Query(new ValidationPipe()) queryParams: QueryParamsWithFilter,
  ): Promise<BirdsListDto> {
    const birdsFindAll = await this.birdService.readAllByLogger(
      req.id,
      queryParams,
    );

    const paginatedList = new BirdsListDto(
      birdsFindAll,
      queryParams.page ?? 1,
      queryParams.perPage ?? 10,
    );

    return paginatedList;
  }

  @UseGuards(JwtAuthGuard)
  @Get("count")
  async findCountAll(@Request() req): Promise<any> {
    const birdsFindAll = await this.birdService.findCountByAccount(req.id);

    return {total: birdsFindAll};
  }

  @UseGuards(JwtAuthGuard)
  @Get("read/:id")
  async findById(@Request() req, @Param() param) {
    const findBird = await this.birdService.findById(param.id);

    return new BirdsToListDTO(
      findBird.id,
      findBird.name,
      findBird.birthdate,
      findBird.rightRing,
      findBird.leftRing,
      findBird.gender,
      findBird.registry,
      findBird.category,
      findBird.breeds,
      findBird.color,
      findBird.status,
      findBird.account,
      findBird.cage,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async create(
    @Request() req,
    @Body(new ValidationPipe())
    birdsToCreate: BirdsCreateDtoIn,
  ): Promise<BirdsCreateDtoOut> {
    const birdCreated = await this.birdService.create(
      req.user.id,
      birdsToCreate,
    );

    return birdCreated;
  }

  @UseGuards(JwtAuthGuard)
  @Put(":id")
  async update(
    @Request() req,
    @Param() param,
    @Body(new ValidationPipe())
    birdsToUpdate: BirdsCreateDtoIn,
  ) {
    const birdUpdated = await this.birdService.update(
      req.id,
      param.id,
      birdsToUpdate,
    );

    return birdUpdated;
  }

  @UseGuards(JwtAuthGuard)
  @Delete(":id")
  @HttpCode(200)
  async delete(@Param() param): Promise<CustomSuccessResponse> {
    await this.birdService.delete(param.id);

    return new CustomSuccessResponse("Passáro excluido com sucesso");
  }
}
