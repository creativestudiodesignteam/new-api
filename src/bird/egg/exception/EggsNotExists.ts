import {AppError} from "src/shared/error/AppError";

export class EggsNotExists extends AppError {
  constructor() {
    super("Ovo inexistente", 404);
  }
}
