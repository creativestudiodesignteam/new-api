import {AppError} from "src/shared/error/AppError";

export class UnauthorizedAuth extends AppError {
  constructor() {
    super("Email ou senha inválidos", 401);
  }
}
