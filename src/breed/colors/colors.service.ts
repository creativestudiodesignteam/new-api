import {Injectable} from "@nestjs/common";
import {ColorsService} from "src/colors/colors.service";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {AppError} from "src/shared/error/AppError";
import {BreedDTO} from "../dto/breed.dto";
import {BreedsColors} from "./colors.entity";
import {BreedsColorsRepository} from "./colors.repository";
import {BreedsColorsDTO} from "./dto/breedsColors";
import {ColorNotExists} from "./exception/colorNotFound";
import {RelationNotFound} from "./exception/RelationNotExists";
import {ColorsObject} from "./interface/ColorsObject";

@Injectable()
export class BreedsColorsService {
  constructor(
    private breedsColorsRepository: BreedsColorsRepository,
    private colorsService: ColorsService,
  ) {}

  async findById(colorsToBeRelation: ColorsObject[]): Promise<any> {
    const checkColor = await Promise.all(
      colorsToBeRelation.map(async result => {
        try {
          const checkColorExists = await this.colorsService.findById(result.id);
          return checkColorExists;
        } catch (err) {
          if (err instanceof AppError) throw new ColorNotExists();
        }
      }),
    );

    return checkColor;
  }

  async findByBreeds(
    breed: BreedDTO,
    queryParams?: QueryParams,
  ): Promise<BreedsColorsDTO[]> {
    const colorsToBreed = await this.breedsColorsRepository.readByBreedWithRelation(
      breed,
      queryParams ?? null,
      ["breed_color", "breed"],
    );
    return colorsToBreed;
  }

  async create(
    breedToBeRelation,
    colorsToBeCreated: ColorsObject[],
  ): Promise<BreedsColors[]> {
    const checkColor = await Promise.all(
      colorsToBeCreated.map(async result => {
        try {
          const colorCheck = await this.colorsService.findById(result.id);

          const breedsColorsCreate = new BreedsColors();
          breedsColorsCreate.breed = breedToBeRelation;
          breedsColorsCreate.breed_color = colorCheck;

          await this.breedsColorsRepository.store(breedsColorsCreate);

          return breedsColorsCreate;
        } catch (err) {
          if (err instanceof AppError) throw new ColorNotExists();
        }
      }),
    );

    return checkColor;
  }

  async findByColorAndBreeds(color, breed): Promise<BreedsColors[]> {
    const checkSearch = await this.breedsColorsRepository.findByWithRelation(
      {
        breed_color: color,
        breed: breed,
      },
      ["breed", "breed_color"],
    );

    return checkSearch;
  }
  async findColorAndBreed(color, breed): Promise<BreedsColors[]> {
    const checkSearch = await this.breedsColorsRepository.findByWithRelation(
      {
        breed_color: color,
        breed: breed,
      },
      ["breed", "breed_color"],
    );

    return checkSearch;
  }

  async delete(id: number) {
    const checkExistsBreedColor = await this.breedsColorsRepository.findById(
      id,
    );

    if (!checkExistsBreedColor) throw new RelationNotFound();

    await this.breedsColorsRepository.destroy(checkExistsBreedColor);
  }
}
