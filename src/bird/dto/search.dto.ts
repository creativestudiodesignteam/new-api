import {AccountDTO} from "src/account/dto/account.dto";
export class SearchParams {
  account?: AccountDTO;
  birthdate?: Date;
  gender?: "Macho" | "Fêmea" | String;
  color?: number;
  category?: number;
  breeds?: number;
  status?: number;
}
