import {Module} from "@nestjs/common";
import {MulterModule} from "@nestjs/platform-express";
import {ImageService} from "./image.service";
import {ImageController} from "./image.controller";
import {ImagesRepository} from "./image.repository";
import {TypeOrmModule} from "@nestjs/typeorm";
import {UploadFilesService} from "src/shared/upload/upload-files.service";

@Module({
  imports: [
    TypeOrmModule.forFeature([ImagesRepository]),
    MulterModule.register({
      dest: "./upload",
    }),
    UploadFilesService,
  ],
  providers: [ImageService, UploadFilesService],
  controllers: [ImageController],
})
export class ImageModule {}
