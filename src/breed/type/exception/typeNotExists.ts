import {AppError} from "src/shared/error/AppError";

export class BreedTypesNotExists extends AppError {
  constructor() {
    super("Este tipo de raça não existe", 404);
  }
}
