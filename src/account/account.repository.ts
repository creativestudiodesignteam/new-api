import {QueryParams} from "src/shared/dto/queryParams.dto";
import {DeepPartial, EntityRepository, Repository} from "typeorm";
import {Account} from "./account.entity";
import {AccountDTO} from "./dto/account.dto";
import {AccountCreateDto} from "./dto/create.dto";

@EntityRepository(Account)
export class AccountRepository extends Repository<Account> {
  async findAll(queryParams: QueryParams): Promise<AccountDTO[]> {
    const users = await this.find({
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      order: {id: "ASC"},
    });

    return users;
  }
  async findById(id: number): Promise<Account> {
    return await this.findOne(id);
  }
  async findBy(paramToSearch) {
    return await this.findOne({where: paramToSearch});
  }
  async store(accountToBeCreated: AccountCreateDto): Promise<any> {
    return await this.insert(accountToBeCreated);
  }
  async updateAccount(Account: DeepPartial<AccountDTO>) {
    return await this.update(Account.id, Account);
  }
  async destroy(Account: Account): Promise<void> {
    await this.remove(Account);
  }
}
