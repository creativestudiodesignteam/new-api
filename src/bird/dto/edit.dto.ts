import {IsNumber, IsString} from "class-validator";
import {AccountDTO} from "src/account/dto/account.dto";
import {BreedDTO} from "src/breed/dto/breed.dto";
import {CageDTO} from "src/cage/dto/cage.dto";
import {CategoriesDTO} from "src/categories/dto/categories.dto";
import {ColorsDTO} from "src/colors/dto/colors.dto";
import {StatusDTO} from "src/status/dto/status.dto";

export class BirdEditDTO {
  @IsNumber()
  id: number;
  name: string;
  birthdate: Date;
  rightRing: string;
  leftRing: string;
  gender: "Macho" | "Fêmea";
  registry: string;
  category: CategoriesDTO;
  breeds: BreedDTO;
  color: ColorsDTO;
  status: StatusDTO;
  account: AccountDTO;
  cage: CageDTO;
}
