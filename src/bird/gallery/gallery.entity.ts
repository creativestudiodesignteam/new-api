import {Account} from "src/account/account.entity";
import {Breeds} from "src/breed/breed.entity";
import {Categories} from "src/categories/categories.entity";
import {Colors} from "src/colors/colors.entity";
import {Status} from "src/status/status.entity";
import {Images} from "src/image/image.entity";

import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import {Birds} from "../bird.entity";

@Entity({name: "birds_galleries"})
export class Gallery {
  @PrimaryGeneratedColumn()
  id: number;
  @ManyToOne(
    () => Images,
    images => images.gallery,
  )
  @JoinColumn({name: "image"})
  image: Images;

  @ManyToOne(
    () => Birds,
    birds => birds.gallery,
  )
  @JoinColumn({name: "bird"})
  bird: Birds;
}
