import {
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Query,
  UseGuards,
  ValidationPipe,
} from "@nestjs/common";
import {JwtAuthGuard} from "src/auth/jwt/jwt-auth.guard";
import {CustomSuccessResponse} from "src/shared/dto/customResponse.dto";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {BreedsColorsService} from "./colors.service";
import {BreedColorsDto} from "./dto/listRelation";

@Controller("breed/color")
export class BreedsColorsController {
  constructor(private breedRepository: BreedsColorsService) {}

  @UseGuards(JwtAuthGuard)
  @Get("read/:breed_id")
  async findByBreed(
    @Query(new ValidationPipe()) queryParams: QueryParams,
    @Param() param,
  ) {
    const colorByBreed = await this.breedRepository.findByBreeds(
      param.breed_id,
      queryParams,
    );

    const paginatedList = new BreedColorsDto(
      colorByBreed,
      queryParams.page ?? 1,
      queryParams.perPage ?? 10,
    );

    return paginatedList;
  }

  @Delete(":id")
  @HttpCode(200)
  async delete(@Param() param): Promise<CustomSuccessResponse> {
    await this.breedRepository.delete(param.id);

    return new CustomSuccessResponse("Raça excluida com sucesso");
  }
}
