import {AppError} from "src/shared/error/AppError";

export class GaleryNotFound extends AppError {
  constructor() {
    super("Galeria inexistente", 404);
  }
}
