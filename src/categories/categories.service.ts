import {Injectable} from "@nestjs/common";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {CategoriesCreateDto} from "./dto/create.dto";
import {CategoriesDTO} from "./dto/categories.dto";
import {CategoriesNotExists} from "./exceptions/categoriesNotExists";
import {Categories} from "./categories.entity";
import {CategoriesRepository} from "./categories.repository";
import { CategoriesEditDto } from "./dto/edit.dto";
import { DeepPartial } from "typeorm";

@Injectable()
export class CategoriesService {
  constructor(private categoriesRepository: CategoriesRepository) {}

  async findAll(queryParams: QueryParams) {
    return await this.categoriesRepository.list(queryParams);
  }

  async findById(id: number): Promise<CategoriesDTO> {
    const CategoriesFinded = await this.categoriesRepository.findById(id);

    if (!CategoriesFinded) throw new CategoriesNotExists();

    return CategoriesFinded;
  }

  async create(categoriesToBeCreated: CategoriesCreateDto): Promise<CategoriesCreateDto> {
    const categories = new Categories();
    categories.name = categoriesToBeCreated.name;

    await this.categoriesRepository.store(categories);

    return categories;
  }

  async update(id: number, categoriesChanges: DeepPartial<CategoriesEditDto>) {
    const categoriesFinded = await this.categoriesRepository.findById(id);

    if (!categoriesFinded) throw new CategoriesNotExists();

    const categories = this.categoriesRepository.create({
      id,
      ...categoriesFinded,
      ...categoriesChanges,
    });

    await this.categoriesRepository.updateCategories(categories);

    return categories;
  }

  async delete(id: number) {
    const categories = await this.categoriesRepository.findById(id);

    if (!categories) throw new CategoriesNotExists();
    this.categoriesRepository.destroy(categories);
  }
}
