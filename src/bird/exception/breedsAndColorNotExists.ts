import {AppError} from "src/shared/error/AppError";

export class BreedsAndColorNotExists extends AppError {
  constructor() {
    super("Verifique as core e raça selecionada", 404);
  }
}
