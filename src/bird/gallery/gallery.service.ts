import {Injectable} from "@nestjs/common";
import {AccountService} from "src/account/account.service";
import {ImagesCreateDTO} from "src/image/dto/create.dto";
import {Images} from "src/image/image.entity";
import {ImageService} from "src/image/image.service";
import {ImageWithLink} from "src/image/dto/imageWithLink";
import {BirdService} from "../bird.service";
import {GalleryCreateDtoIn} from "./dto/create.dto";
import {GalleryDTO} from "./dto/gallery.dto";
import {GalleryRepository} from "./gallery.repository";
import {AppError} from "src/shared/error/AppError";
import {FilesNotSend} from "./exception/filesNotSend";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {BirdsNotExists} from "../exception/BirdNotExists";
import {GaleryNotFound} from "./exception/imageToRelation";
import {UploadFilesService} from "src/shared/upload/upload-files.service";
import {NotPermission} from "./exception/notPermission";
import {check} from "prettier";

@Injectable()
export class GalleryService {
  constructor(
    private galleryRepository: GalleryRepository,
    private imageService: ImageService,
    private birdService: BirdService,
    private accountService: AccountService,
  ) {}

  async findAll(id_bird: number, queryParams: QueryParams): Promise<any> {
    const checkBird = await this.birdService.findById(id_bird);

    const findedAllBirds = await this.galleryRepository.findAllByWithRelation(
      {
        bird: checkBird,
      },
      ["image"],
      queryParams,
    );

    if (!findedAllBirds) throw new BirdsNotExists();

    return findedAllBirds;
  }

  async create(account, bird, filesTobeCreate): Promise<any> {
    if (filesTobeCreate.files.length <= 0) throw new FilesNotSend();
    const birds = await this.birdService.findByIdAccount(account, bird);
    const createdImages = await Promise.all(
      await filesTobeCreate.files.map(
        async (result): Promise<ImageWithLink> => {
          const images = new ImagesCreateDTO();
          images.filename = result.filename;
          images.destination = result.destination;

          return await this.imageService.create(images);
        },
      ),
    );

    const createdGallery = await Promise.all(
      createdImages.map(async (result: ImageWithLink) => {
        const gallery = new GalleryCreateDtoIn();
        gallery.image = result;
        gallery.bird = birds;

        await this.galleryRepository.store(gallery);
        return gallery;
      }),
    );
    const gallery = await Promise.all(createdGallery);

    return gallery;
  }

  async delete(account: number, id: number) {
    const checkAccount = await this.accountService.findById(account);

    const findedImages = await this.galleryRepository.findByWithRelation(
      {
        id,
      },
      ["bird", "image"],
    );

    if (!findedImages) throw new GaleryNotFound();

    console.log(findedImages.bird);

    const checkBird = await this.birdService.findByIdAccount(
      checkAccount.id,
      findedImages.bird.id,
    );

    if (!checkBird) throw new NotPermission();

    await this.galleryRepository.destroy(findedImages);
    await this.imageService.delete(findedImages.image.id);
  }
}
