import {IsDate, IsNumber, IsNumberString, IsOptional} from "class-validator";
export class BabiesEditDto {
  id: number;
  @IsOptional()
  birthdate: Date;

  @IsOptional()
  @IsNumber()
  cage: number;

  @IsOptional()
  @IsNumber()
  status: number;

  @IsOptional()
  @IsNumber()
  mother: number;

  @IsOptional()
  @IsNumber()
  father: number;
}
