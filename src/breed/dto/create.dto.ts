import {CategoriesDTO} from "src/categories/dto/categories.dto";
import {ColorsDTO} from "src/colors/dto/colors.dto";
import {BreedsTypesDTO} from "../type/dto/types.dto";

export class BreedsCreateDto {
  id: number;
  name: string;
  breed_type: BreedsTypesDTO;
  category: CategoriesDTO;
  colors?: ColorsDTO[];
}
