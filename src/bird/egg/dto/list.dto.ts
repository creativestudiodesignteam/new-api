import {PaginatedList} from "src/shared/interface/paginated";
import {EggsDTO} from "./egg.dto";

export class EggsListDto implements PaginatedList {
  eggs: EggsDTO[];
  page: number;
  perPage: number;

  constructor(eggs: EggsDTO[], page: number, perPage: number) {
    const eggsFormat = eggs.map(result => {
      const eggDTO = new EggsDTO(
        result.id,
        result.birthdate,
        result.layingDate,
        result.hatchingDate,
        result.candlingDate,
        result.isShocked,
        result.state,
        result.account,
        result.cage,
        result.parents,
      );
      delete eggDTO.account.password;

      return eggDTO;
    });

    this.eggs = eggsFormat;
    this.page = page;
    this.perPage = perPage;
  }

  public getPaginatedData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      Eggs: this.eggs,
    };
  }
}
