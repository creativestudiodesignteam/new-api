import {PaginatedList} from "./../../shared/interface/paginated";
import {BirdsToListDTO} from "./birdsToList.dto";

export class BirdsListDto implements PaginatedList {
  birds: BirdsToListDTO[];
  page: number;
  perPage: number;

  constructor(birds: BirdsToListDTO[], page: number, perPage: number) {
    this.birds = birds;
    this.page = page;
    this.perPage = perPage;
  }

  public getPaginatedData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      Birds: this.birds,
    };
  }
}
