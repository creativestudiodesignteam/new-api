import {AppError} from "src/shared/error/AppError";

export class ColorsNotExists extends AppError {
  constructor() {
    super("Colors inexisteste", 404);
  }
}
