import {AppError} from "src/shared/error/AppError";

export class FileNotFound extends AppError {
  constructor() {
    super("Arquivo inexistente", 404);
  }
}
