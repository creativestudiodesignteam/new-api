import {Module} from "@nestjs/common";
import {BreedService} from "./breed.service";
import {BreedController} from "./breed.controller";
import {BreedTypeService} from "./type/type.service";
import {TypeController} from "./type/type.controller";
import {TypeOrmModule} from "@nestjs/typeorm";
import {BreedTypeRepository} from "./type/type.repository";
import {BreedsRepository} from "./breed.repository";
import {CategoriesModule} from "src/categories/categories.module";
import {CategoriesService} from "src/categories/categories.service";
import {CategoriesRepository} from "src/categories/categories.repository";
import {BreedsColorsService} from "./colors/colors.service";
import {BreedsColorsRepository} from "./colors/colors.repository";
import {ColorsModule} from "src/colors/colors.module";
import {ColorsService} from "src/colors/colors.service";
import {ColorsRepository} from "src/colors/colors.repository";
import {BreedsColorsController} from "./colors/colors.controller";

@Module({
  imports: [
    TypeOrmModule.forFeature([
      BreedTypeRepository,
      BreedsRepository,
      CategoriesRepository,
      BreedsColorsRepository,
      ColorsRepository,
    ]),
    ColorsModule,
    CategoriesModule,
  ],
  providers: [
    BreedService,
    BreedTypeService,
    CategoriesService,
    ColorsService,
    BreedsColorsService,
  ],
  controllers: [BreedController, TypeController, BreedsColorsController],
})
export class BreedModule {}
