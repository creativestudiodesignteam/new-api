import {Colors} from "src/colors/colors.entity";
import {BreedsColors} from "../colors.entity";

export class BreedsColorsDTO {
  id: number;
  name: string;
  breedColors: BreedsColors;
  colors: Colors;

  constructor(id: number, breedColors: BreedsColors, colors: Colors) {
    this.id = id;
    this.breedColors = breedColors;
    this.colors = colors;
  }
}
