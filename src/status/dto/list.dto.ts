import {StatusDTO} from "./status.dto";

export class StatusListDto {
  Status: StatusDTO[];
  page: number;
  perPage: number;

  constructor(status: StatusDTO[], page: number, perPage: number) {
    this.Status = status;
    this.page = page;
    this.perPage = perPage;
  }

  public getStatusData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      Status: this.Status,
    };
  }
}
