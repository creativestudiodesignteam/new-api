import {AppError} from "src/shared/error/AppError";

export class BreedTypesAlreadyExists extends AppError {
  constructor() {
    super("Este tipo de raça já existe", 400);
  }
}
