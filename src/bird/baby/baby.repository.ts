import {DeepPartial, EntityRepository, Repository} from "typeorm";

import {QueryParams} from "src/shared/dto/queryParams.dto";
import {Babies} from "./baby.entity";
import {AllowedBabiesRelation} from "./interface/allowedBabiesRelations";

@EntityRepository(Babies)
export class BabiesRepository extends Repository<Babies> {
  async findAll(queryParams: QueryParams): Promise<Babies[]> {
    const babies = await this.find({
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      order: {id: "ASC"},
    });

    return babies;
  }

  async findById(id: number): Promise<any> {
    return await this.findOne(id);
  }

  async findByWithRelation(paramToSearch, relation: AllowedBabiesRelation[]) {
    return this.findOne({where: paramToSearch, relations: relation});
  }

  async findWithRelation(
    paramToSearch,
    queryParams: QueryParams,
    relation: AllowedBabiesRelation[],
  ) {
    return this.find({
      where: paramToSearch,
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      relations: relation,
    });
  }

  async findBy(paramToSearch) {
    return await this.findOne({where: paramToSearch});
  }

  async store(babiesToBeCreated: any): Promise<any> {
    return await this.insert(babiesToBeCreated);
  }

  async updateBabies(babies: DeepPartial<any>) {
    return await this.update(babies.id, babies);
  }

  async destroy(babies: any): Promise<void> {
    await this.remove(babies);
  }
}
