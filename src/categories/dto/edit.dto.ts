import {IsString} from "class-validator";

export class CategoriesEditDto {
  @IsString()
  name: string;
}
