import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("images", table => {
    table.increments("id").primary();
    table.string("name", 100).notNullable();
    table.string("path", 100).notNullable();
    table.timestamps(undefined, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("images");
}
