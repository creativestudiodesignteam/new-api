import {AccountDTO} from "src/account/dto/account.dto";
import {Account} from "../../account/account.entity";
export class CageDTO {
  id: number;
  name: string;
  state?: boolean;
  account: AccountDTO;
  created_at?: Date;
  updated_at?: Date;

  constructor(
    id: number,
    name: string,
    state: boolean,
    account: AccountDTO,
    created_at?: Date,
    updated_at?: Date,
  ) {
    this.id = id;
    this.name = name;
    this.state = state ?? true;
    this.account = account;
    this.created_at = created_at ?? undefined;
    this.updated_at = updated_at ?? undefined;
  }
}
