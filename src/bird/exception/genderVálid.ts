import {AppError} from "src/shared/error/AppError";

export class GenderInvalid extends AppError {
  constructor() {
    super("Gênero inválido", 404);
  }
}
