import {IsString} from "class-validator";
import {Breeds} from "src/breed/breed.entity";
import {Categories} from "src/categories/categories.entity";
import {CategoriesDTO} from "src/categories/dto/categories.dto";
import {Colors} from "src/colors/colors.entity";
import {ColorsDTO} from "src/colors/dto/colors.dto";
import {Images} from "src/image/image.entity";
import {Status} from "src/status/status.entity";

export class BirdsCreateDtoIn {
  id: number;
  @IsString()
  name: string;
  birthdate: Date;
  @IsString()
  rightRing: string;
  @IsString()
  leftRing: string;
  @IsString()
  gender: "Macho" | "Fêmea";
  @IsString()
  registry: string;
  category: number;
  breeds: number;
  color: number;
  status: number;
  cages: number;
  images: number;
  mother: number;
  father: number;
}
