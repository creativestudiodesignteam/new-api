import {AppError} from "src/shared/error/AppError";

export class FileNotSend extends AppError {
  constructor() {
    super("Arquivo de imagem não enviado, ou formato está inválido", 400);
  }
}
