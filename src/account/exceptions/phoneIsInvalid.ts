import {AppError} from "src/shared/error/AppError";

export class PhoneIsInvalid extends AppError {
  constructor() {
    super("Número de telephone inválido", 400);
  }
}
