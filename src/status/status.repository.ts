import {EntityRepository, Repository} from "typeorm";
import {Status} from "./status.entity";
import {StatusDTO} from "./dto/status.dto";
import {StatusCreateDto} from "./dto/create.dto";
import { QueryParams } from "src/shared/dto/queryParams.dto";

@EntityRepository(Status)
export class StatusRepository extends Repository<Status> {
  async list(queryParams: QueryParams): Promise<StatusDTO[]> {
    const status = await this.find({
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      order: {id: "ASC"},
    });

    return status;
  }

  async findById(id: number): Promise<Status> {
    return await this.findOne(id);
  }
  
  async store(statusToBeCreated: StatusCreateDto): Promise<any> {
    return await this.insert(statusToBeCreated);
  }

  async destroy(Status: Status): Promise<void> {
    await this.remove(Status);
  }
}
