import {Injectable} from "@nestjs/common";
import {query} from "express";
import {AccountService} from "src/account/account.service";
import {CageService} from "src/cage/cage.service";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {AppError} from "src/shared/error/AppError";
import {StatusService} from "src/status/status.service";
import {BirdService} from "../bird.service";
import {ParentsCreateDto} from "../parent/dto/create.dto";
import {ParentService} from "../parent/parent.service";
import {BabiesRepository} from "./baby.repository";
import {BabiesCreateDto} from "./dto/create.dto";
import {BabiesOutCreateDto} from "./dto/createOut.dto";
import {BabiesEditDto} from "./dto/edit.dto";
import {BabiesRingDto} from "./dto/ringToUpdate.dto";
import {ChildrenNotExists} from "./exception/ChildrenNotExists";
import {now} from "moment";
import {BirdsCreateDtoIn} from "../dto/create-in.dto";

@Injectable()
export class BabyService {
  constructor(
    private babiesRepository: BabiesRepository,
    private cageService: CageService,
    private statusService: StatusService,
    private parentService: ParentService,
    private accountService: AccountService,
    private birdService: BirdService,
  ) {}

  async transformToBird(account: number, id: number, birdToTransform) {
    const checkAccount = await this.accountService.findById(account);

    const checkBaby = await this.babiesRepository.findByWithRelation(
      {
        id,
        account: checkAccount,
      },
      ["cage", "parents", "status"],
    );

    if (!checkBaby) throw new ChildrenNotExists();

    if (checkBaby.parents) {
      var checkParents = await this.parentService.findById(
        checkBaby.parents.id,
      );
    }

    const birds = new BirdsCreateDtoIn();
    birds.name = birdToTransform.name;
    birds.birthdate = checkBaby.birthdate;
    birds.rightRing = birdToTransform.rightRing ?? checkBaby.rightRing;
    birds.leftRing = birdToTransform.rightRing ?? checkBaby.leftRing;
    birds.gender = birdToTransform.gender;
    birds.registry = birdToTransform.registry;
    birds.category = birdToTransform.category;
    birds.breeds = birdToTransform.breeds;
    birds.color = birdToTransform.color;
    birds.status = birdToTransform.status;
    birds.cages = birdToTransform.cages ?? checkBaby.cage.id;
    birds.mother = checkParents ? checkParents.mother.id : null;
    birds.father = checkParents ? checkParents.father.id : null;

    const transformToBird = await this.birdService.create(account, birds);

    return transformToBird;
  }

  async separetedToBaby(account: number, id: number) {
    const checkAccount = await this.accountService.findById(account);

    const checkBaby = await this.babiesRepository.findByWithRelation(
      {
        id,
        account: checkAccount,
      },
      ["cage", "parents", "status"],
    );

    if (!checkBaby) throw new ChildrenNotExists();

    if (checkBaby.separete_date)
      throw new AppError("Este filhote já foi separado");

    const babyUpdated = this.babiesRepository.create({
      id: id,
      ...checkBaby,
      separete_date: new Date(now()),
    });

    await this.babiesRepository.updateBabies(babyUpdated);

    return babyUpdated;
  }

  async ringToBaby(account: number, id: number, ringToUpdate: BabiesRingDto) {
    const checkAccount = await this.accountService.findById(account);

    const checkBaby = await this.babiesRepository.findByWithRelation(
      {
        id,
        account: checkAccount,
      },
      ["cage", "parents", "status"],
    );

    if (!checkBaby) throw new ChildrenNotExists();

    const babyUpdated = this.babiesRepository.create({
      id: id,
      ...checkBaby,
      rightRing: ringToUpdate.leftRing,
      leftRing: ringToUpdate.rightRing,
      ring_date: ringToUpdate.ring_date ?? new Date(now()),
    });

    await this.babiesRepository.updateBabies(babyUpdated);

    return babyUpdated;
  }

  async findAll(queryParams: QueryParams) {
    return await this.babiesRepository.findAll(queryParams);
  }

  async findAllByCages(cage: number, queryParams: QueryParams) {
    const checkCages = await this.cageService.findByIdWithoutAccount(cage);

    const findedBaby = await this.babiesRepository.findWithRelation(
      {cage: checkCages},
      queryParams,
      ["cage", "parents", "status"],
    );

    return findedBaby;
  }

  async findById(id: number) {
    const checkBaby = await this.babiesRepository.findByWithRelation({id}, [
      "status",
      "parents",
      "cage",
    ]);

    if (!checkBaby) throw new ChildrenNotExists();

    return checkBaby;
  }

  async create(account: number, babiesToBeCreate: BabiesCreateDto) {
    const checkCages = await this.cageService.findById(
      babiesToBeCreate.cage,
      account,
    );

    const checkAccount = await this.accountService.findById(account);

    const checkStatus = await this.statusService.findById(
      babiesToBeCreate.status,
    );

    if (babiesToBeCreate.father && babiesToBeCreate.mother) {
      const parents = new ParentsCreateDto();
      parents.father = babiesToBeCreate.father;
      parents.mother = babiesToBeCreate.mother;

      var createdParents = await this.parentService.create(parents);
    }
    const babies = new BabiesOutCreateDto();

    babies.birthdate = babiesToBeCreate.birthdate;
    babies.status = checkStatus;
    babies.cage = checkCages;
    babies.parents = createdParents ?? null;
    babies.account = checkAccount;

    await this.babiesRepository.store(babies);

    return babies;
  }

  async update(account: number, id: number, babiesToBeCreate: BabiesEditDto) {
    const checkAccount = await this.accountService.findById(account);

    const checkBaby = await this.babiesRepository.findByWithRelation(
      {
        id,
        account: checkAccount,
      },
      ["status", "cage", "parents"],
    );

    if (!checkBaby) throw new ChildrenNotExists();

    if (babiesToBeCreate.cage) {
      var checkCages = await this.cageService.findById(
        babiesToBeCreate.cage,
        account,
      );
    }

    if (babiesToBeCreate.status) {
      var checkStatus = await this.statusService.findById(
        babiesToBeCreate.status,
      );
    }

    if (babiesToBeCreate.mother || babiesToBeCreate.father) {
      var parentsUpd = new ParentsCreateDto();
      parentsUpd.father = babiesToBeCreate.father;
      parentsUpd.mother = babiesToBeCreate.mother;

      var parentUpdated = await this.parentService.update(
        checkBaby.parents.id,
        parentsUpd,
      );
    }

    const parents = this.babiesRepository.create({
      id: babiesToBeCreate.id,
      ...checkBaby,
      birthdate: babiesToBeCreate.birthdate,
      cage: checkCages,
      status: checkStatus,
      parents: parentUpdated,
    });

    await this.babiesRepository.updateBabies(parents);

    return parents;
  }

  async delete(id: number) {
    const babie = await this.babiesRepository.findById(id);

    if (!babie) throw new ChildrenNotExists();
    this.babiesRepository.destroy(babie);
  }
}
