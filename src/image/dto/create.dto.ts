import {IsString} from "class-validator";
export class ImagesCreateDTO {
  @IsString()
  filename: string;
  @IsString()
  destination: string;
}
