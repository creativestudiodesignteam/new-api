import {AppError} from "src/shared/error/AppError";

export class ColorNotExists extends AppError {
  constructor() {
    super(
      "Algumas cores selecionadas, já estão cadastrada a esta raça, tente novamente",
      404,
    );
  }
}
