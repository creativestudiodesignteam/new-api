import {IsDate, IsNumber, IsNumberString, IsOptional} from "class-validator";
import { AccountDTO } from "src/account/dto/account.dto";
import { ParentsDTO } from "src/bird/parent/dto/parent.dto";
import { CageDTO } from "src/cage/dto/cage.dto";
export class EggsCreateDto {
  id: number;
  birthdate: Date;
  layingDate: Date;
  state: String;
  account: AccountDTO;
  cage: CageDTO;
  parents: ParentsDTO;
}
