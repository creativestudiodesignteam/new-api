import {Birds} from "src/bird/bird.entity";
import {Breeds} from "src/breed/breed.entity";
import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";

@Entity({name: "categories"})
export class Categories {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(
    () => Breeds,
    breeds => breeds.category,
  )
  breeds: Breeds;

  @OneToMany(
    () => Birds,
    bird => bird.category,
  )
  birds: Birds;
}
