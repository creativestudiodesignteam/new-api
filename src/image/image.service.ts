import {Injectable} from "@nestjs/common";
import {AppError} from "src/shared/error/AppError";
import {UploadFilesService} from "src/shared/upload/upload-files.service";
import {ImagesRepository} from "./image.repository";
import {Images} from "./image.entity";
import {ImagesCreateDTO} from "./dto/create.dto";
import {ImagesListDto} from "./dto/list.dto";
import {DeepPartial} from "typeorm";
import {check} from "prettier";
import {join} from "path";
import {existsSync} from "fs";
import {FileNotFound} from "./exception/fileNotFound";
import {FileNotSend} from "./exception/fileNotSend";
import {FilenameExists} from "./exception/fileNameExists";

@Injectable()
export class ImageService {
  constructor(
    private imagesRepository: ImagesRepository,
    private uploadFilesService: UploadFilesService,
  ) {}

  async getImage(filename): Promise<any> {
    const checkImageNameExists = await this.imagesRepository.findBy({
      name: filename,
    });

    const pathDestinationFormatter = checkImageNameExists.path
      .split("/")
      .slice(1)
      .join("/");

    const filePath = join(pathDestinationFormatter, filename);
    const checkFileExists = existsSync(filePath);

    if (!checkFileExists) {
      throw new FileNotFound();
    }

    return {name: filename, path: pathDestinationFormatter};
  }

  async findById(id: number) {
    const checkImageExists = await this.imagesRepository.findById(id);

    if (!checkImageExists) return new FileNotFound();

    return new ImagesListDto(checkImageExists);
  }

  async create(fileTobeCreate: ImagesCreateDTO): Promise<any> {
    if (!fileTobeCreate) {
      throw new FileNotSend();
    }

    const checkImageNameExists = await this.imagesRepository.findBy({
      name: fileTobeCreate.filename,
      path: fileTobeCreate.destination,
    });

    if (checkImageNameExists) {
      await this.uploadFilesService.delete(
        fileTobeCreate.destination,
        fileTobeCreate.filename,
      );

      throw new FilenameExists();
    }
    const imagePathTransformer = fileTobeCreate.destination.split(".")[1];

    const images = new Images();
    images.name = fileTobeCreate.filename;
    images.path = imagePathTransformer;

    await this.imagesRepository.store(images);

    return new ImagesListDto(images);
  }

  async update(id: number, fileTobeUpdate) {
    const checkImageExists = await this.imagesRepository.findById(id);

    if (!checkImageExists) {
      await this.uploadFilesService.delete(
        checkImageExists.path,
        checkImageExists.name,
      );

      return new ImagesListDto(checkImageExists);
    }

    if (
      fileTobeUpdate.filename &&
      fileTobeUpdate.filename !== checkImageExists.name
    ) {
      await this.uploadFilesService.delete(
        `.${checkImageExists.path}/`,
        checkImageExists.name,
      );
    }

    fileTobeUpdate.name = fileTobeUpdate.filename;
    const imagesChange = this.imagesRepository.create({
      id,
      ...checkImageExists,
      name: fileTobeUpdate.name,
    });

    await this.imagesRepository.updateImages(imagesChange);

    return imagesChange;
  }

  async delete(id: number) {
    const findedImages = await this.imagesRepository.findBy({
      id,
    });

    if (!findedImages) throw new FileNotFound();

    this.uploadFilesService.delete(`.${findedImages.path}/`, findedImages.name);

    this.imagesRepository.destroy(findedImages);
  }
}
