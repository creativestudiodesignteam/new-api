import {BreedsColorsDTO} from "./breedsColors";
export class BreedColorsDto {
  breedsColors: BreedsColorsDTO[];
  page: number;
  perPage: number;

  constructor(breedsColors: BreedsColorsDTO[], page: number, perPage: number) {
    this.breedsColors = breedsColors;
    this.page = page;
    this.perPage = perPage;
  }

  public getBreedsData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      breeds_colors: this.breedsColors,
    };
  }
}
