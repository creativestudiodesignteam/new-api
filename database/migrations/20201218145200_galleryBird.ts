import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("birds_galleries", table => {
    table.increments("id").primary();
    table
      .integer("image")
      .references("id")
      .inTable("images")
      .notNullable();
    table
      .integer("bird")
      .references("id")
      .inTable("birds")
      .notNullable();
    table.timestamps(undefined, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("birds_galleries");
}
