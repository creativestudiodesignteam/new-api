import {Account} from "src/account/account.entity";
import {Status} from "src/status/status.entity";
import {Cage} from "src/cage/cage.entity";

import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import {Parent} from "../parent/parent.entity";
import {ParentsDTO} from "../parent/dto/parent.dto";
import {AccountDTO} from "src/account/dto/account.dto";
import {CageDTO} from "src/cage/dto/cage.dto";
import {StatusDTO} from "src/status/dto/status.dto";

@Entity({name: "birds_babies"})
export class Babies {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  rightRing: string;

  @Column()
  leftRing: string;

  @Column()
  ring_date: Date;
  
  @Column()
  birthdate: Date;



  @Column()
  separete_date: Date;

  @ManyToOne(
    () => Account,
    accounts => accounts.babies,
  )
  @JoinColumn({name: "account"})
  account: AccountDTO;

  @ManyToOne(
    () => Cage,
    cages => cages.babies,
  )
  @JoinColumn({name: "cages"})
  cage: CageDTO;

  @ManyToOne(
    () => Status,
    status => status.babies,
  )
  @JoinColumn({name: "status"})
  status: StatusDTO;

  @ManyToOne(
    () => Parent,
    parent => parent.babies,
  )
  @JoinColumn({name: "parents"})
  parents: ParentsDTO;
}
