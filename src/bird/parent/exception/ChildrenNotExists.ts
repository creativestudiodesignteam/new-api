import {AppError} from "src/shared/error/AppError";

export class ChildrenNotExists extends AppError {
  constructor() {
    super("Passáro filho inexistente", 400);
  }
}
