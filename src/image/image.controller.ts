import {
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
  Query,
  Req,
  Res,
  UploadedFile,
  UseInterceptors,
  ValidationPipe,
} from "@nestjs/common";
import {FileFieldsInterceptor, FileInterceptor} from "@nestjs/platform-express";
import {diskStorage} from "multer";
import {join} from "path";
import {of} from "rxjs";
import {CustomSuccessResponse} from "src/shared/dto/customResponse.dto";
import {editFileName} from "src/shared/upload/utils/upload-files";
import {ImageService} from "./image.service";
@Controller("files")
export class ImageController {
  constructor(private imagesRepository: ImageService) {}

  @Get("read/:filename")
  async getFiles(@Param() param, @Res() res) {
    const filePath = await this.imagesRepository.getImage(param.filename);
    return of(
      res.sendFile(join(process.cwd(), `${filePath.path}/${filePath.name}`)),
    );
  }

  @Get("/find/:id")
  async findById(@Param() param) {
    const findPostByUser = await this.imagesRepository.findById(param.id);
    return findPostByUser;
  }

  @Post()
  @UseInterceptors(
    FileInterceptor("file", {
      fileFilter: (req, file, cb) => {
        if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
          req.fileErrors = {
            error: true,
          };
          return cb(null, false);
        }
        return cb(null, true);
      },
      storage: diskStorage({
        destination: "./upload",
        filename: editFileName,
      }),
    }),
  )
  async create(@Req() req, @UploadedFile() file) {
    console.log(req);
    const uploadImage = await this.imagesRepository.create(file);
    return uploadImage;
  }

  @Put(":id")
  @UseInterceptors(
    FileInterceptor("file", {
      fileFilter: (req, file, cb) => {
        if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
          req.fileErrors = {
            error: true,
          };
          return cb(null, false);
        }
        return cb(null, true);
      },
      storage: diskStorage({
        destination: "./upload",
        filename: editFileName,
      }),
    }),
  )
  async update(@Param() param, @UploadedFile() file) {
    const uploadImage = await this.imagesRepository.update(param.id, file);
    return uploadImage;
  }

  @Delete(":id")
  @HttpCode(200)
  async delete(@Param() param): Promise<CustomSuccessResponse> {
    await this.imagesRepository.delete(param.id);
    return new CustomSuccessResponse("Desafio excluído com sucesso");
  }
}
