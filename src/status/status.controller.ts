import {ValidationPipe} from "./../shared/pipes/validation.pipe";
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Query,
  Post,
} from "@nestjs/common";
import {StatusCreateDto} from "./dto/create.dto";

// import {ApiTags} from "@nestjs/swagger";

import {StatusListDto} from "./dto/list.dto";
import {CustomSuccessResponse} from "src/shared/dto/customResponse.dto";
import {StatusService} from "./status.service";
import {StatusDTO} from "./dto/status.dto";
import {QueryParams} from "src/shared/dto/queryParams.dto";

// @ApiTags("Status")
@Controller("status")
export class StatusController {
  constructor(private statusService: StatusService) {}

  @Get()
  async findAll(
    @Query(new ValidationPipe()) queryParams: QueryParams,
  ): Promise<any> {
    const StatusFindAll = await this.statusService.findAll(queryParams);    

    const paginatedList = new StatusListDto(
      StatusFindAll,
      queryParams.page ?? 1,
      queryParams.perPage ?? 10,
    );

    return paginatedList;
  }

  @Get(":id")
  async findById(@Param() param): Promise<StatusDTO> {
    const StatusFindById = await this.statusService.findById(param.id);
    return StatusFindById;
  }

  @Post()
  async create(@Body(new ValidationPipe()) status: StatusCreateDto) {
    const StatusCreate = await this.statusService.create(status);
    return StatusCreate;
  }

  @Delete()
  @HttpCode(200)
  async delete(@Param() param): Promise<CustomSuccessResponse> {
    await this.statusService.delete(param.id);

    return new CustomSuccessResponse("Status excluido com sucesso");
  }
}
