import {AppError} from "src/shared/error/AppError";

export class StatusNotExists extends AppError {
  constructor() {
    super("Status inexisteste", 404);
  }
}
