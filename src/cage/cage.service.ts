import {Injectable} from "@nestjs/common";
import {AccountService} from "src/account/account.service";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {AppError} from "src/shared/error/AppError";
import {DeepPartial} from "typeorm";
import {Cage} from "./cage.entity";
import {CagesRepository} from "./cage.repository";
import {CageDTO} from "./dto/cage.dto";
import {CageCreateDto} from "./dto/create.dto";
import {CageEditDto} from "./dto/edit.dto";
import {CageNotExists} from "./exceptions/cageNotExists";

@Injectable()
export class CageService {
  constructor(
    private cagesRepository: CagesRepository,
    private accountService: AccountService,
  ) {}

  async findByUser(id_account: number, queryParams: QueryParams) {
    const checkAccount = await this.accountService.findById(id_account);

    if (!checkAccount)
      throw new AppError("Conta inexistente, faça o login, e tente novamente");

    return await this.cagesRepository.findAllByUser(checkAccount, queryParams);
  }

  async findById(id: number, id_account: number) {
    const checkAccount = await this.accountService.findById(id_account);

    const checkCages = await this.cagesRepository.findBy({
      id,
      account: checkAccount,
    });

    if (!checkCages) throw new CageNotExists();

    return checkCages;
  }

  async findByIdWithoutAccount(id: number) {
    const checkCages = await this.cagesRepository.findBy({
      id,
    });

    if (!checkCages) throw new CageNotExists();

    return checkCages;
  }

  async create(id_account: number): Promise<any> {
    const checkAccount = await this.accountService.findById(id_account);

    if (!checkAccount)
      throw new AppError("Conta inexistente, faça o login, e tente novamente");

    const cage = new CageCreateDto();

    const cageNumber = await this.cagesRepository.findAllByUserWithoutPagination(
      checkAccount,
    );

    // eslint-disable-next-line prefer-const
    let cages = [];
    let nameCage = "";
    cageNumber.forEach(element => {
      if (!!+element["name"]) {
        cages.push(element);
      }
    });

    if (cages.length == 0) {
      nameCage = "1";
    } else {
      for (let i = 0; i < cages.length; i++) {
        if (cages[0].name != "1") {
          nameCage = "1";
          break;
        } else if (cages.length - 1 == i) {
          nameCage = (parseInt(cages[i].name) + 1).toString();
          break;
        } else if (parseInt(cages[i + 1].name) - parseInt(cages[i].name) > 1) {
          nameCage = (i + 2).toString();
          break;
        }
      }
    }

    cage.name = nameCage;
    cage.account = checkAccount;
    cage.state = true;

    await this.cagesRepository.store(cage);

    return cage;
  }

  async update(
    id: number,
    id_account: number,
    cagesChanges: DeepPartial<CageEditDto>,
  ) {
    const cagesFinded = await this.cagesRepository.findById(id);

    if (!cagesFinded) throw new CageNotExists();

    const cages = this.cagesRepository.create({
      id,
      ...cagesFinded,
      ...cagesChanges,
    });

    await this.cagesRepository.updateCages(cages);

    return cages;
  }

  async delete(id: number) {
    const cages = await this.cagesRepository.findById(id);

    if (!cages) throw new CageNotExists();
    this.cagesRepository.destroy(cages);
  }
}
