import {
  Body,
  Controller,
  Post,
  UseGuards,
  Request,
  Put,
  Param,
  Req,
  Delete,
  HttpCode,
  Get,
  Query,
  Patch,
} from "@nestjs/common";
import {JwtAuthGuard} from "src/auth/jwt/jwt-auth.guard";
import {CustomSuccessResponse} from "src/shared/dto/customResponse.dto";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {ValidationPipe} from "src/shared/pipes/validation.pipe";
import {BirdsDTO} from "../dto/birds.dto";
import {BirdsCreateDtoOut} from "../dto/create-out.dto";
import {BabyService} from "./baby.service";
import {BabiesCreateDto} from "./dto/create.dto";
import {BabiesEditDto} from "./dto/edit.dto";
import {BabiesListDto} from "./dto/list.dto";
import {BabiesRingDto} from "./dto/ringToUpdate.dto";
import {TransformToBirdDto} from "./dto/transform.dto";

@Controller("baby")
export class BabyController {
  constructor(private babyService: BabyService) {}

  @UseGuards(JwtAuthGuard)
  @Get("cages/read/:id")
  async findByCages(
    @Request() req,
    @Param() param,
    @Query(new ValidationPipe()) queryParams: QueryParams,
  ) {
    const babiesFindAll = await this.babyService.findAllByCages(
      param.id,
      queryParams,
    );

    const paginatedList = new BabiesListDto(
      babiesFindAll,
      queryParams.page ?? 1,
      queryParams.perPage ?? 10,
    );

    return paginatedList;
  }

  @UseGuards(JwtAuthGuard)
  @Get("read")
  async findAll(
    @Request() req,
    @Query(new ValidationPipe()) queryParams: QueryParams,
  ) {
    const babiesFindAll = await this.babyService.findAll(queryParams);

    const paginatedList = new BabiesListDto(
      babiesFindAll,
      queryParams.page ?? 1,
      queryParams.perPage ?? 10,
    );

    return paginatedList;
  }

  @UseGuards(JwtAuthGuard)
  @Get("read/:id")
  async findById(@Request() req, @Param() param) {
    const findBird = await this.babyService.findById(param.id);

    return findBird;
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async create(
    @Request() req,
    @Body(new ValidationPipe())
    babiesToCreate: BabiesCreateDto,
  ): Promise<any> {
    const babieCreated = await this.babyService.create(
      req.user.id,
      babiesToCreate,
    );

    return babieCreated;
  }

  @UseGuards(JwtAuthGuard)
  @Post("transform/bird/:id")
  async transformToBird(
    @Request() req,
    @Param() param,
    @Body(new ValidationPipe())
    babiesToCreate: TransformToBirdDto,
  ): Promise<BirdsCreateDtoOut> {
    const babieCreated = await this.babyService.transformToBird(
      req.user.id,
      param.id,
      babiesToCreate,
    );

    return babieCreated;
  }

  @UseGuards(JwtAuthGuard)
  @Put(":id")
  async update(
    @Request() req,
    @Param() param,
    @Body(new ValidationPipe())
    parentsToUpdate: BabiesEditDto,
  ) {
    const babiesUpdated = await this.babyService.update(
      req.user.id,
      param.id,
      parentsToUpdate,
    );

    return babiesUpdated;
  }

  @UseGuards(JwtAuthGuard)
  @Patch("rings/:id")
  async updateToRings(
    @Request() req,
    @Param() param,
    @Body(new ValidationPipe())
    babyToUpdateRings: BabiesRingDto,
  ) {
    const babiesUpdated = await this.babyService.ringToBaby(
      req.user.id,
      param.id,
      babyToUpdateRings,
    );

    return babiesUpdated;
  }

  @UseGuards(JwtAuthGuard)
  @Patch("separate/:id")
  async separetedBaby(@Request() req, @Param() param) {
    const babiesUpdated = await this.babyService.separetedToBaby(
      req.user.id,
      param.id,
    );

    return babiesUpdated;
  }

  @Delete(":id")
  @HttpCode(200)
  async delete(@Param() param): Promise<CustomSuccessResponse> {
    await this.babyService.delete(param.id);

    return new CustomSuccessResponse("Filhote excluidos com sucesso");
  }
}
