import {Strategy} from "passport-local";
import {PassportStrategy} from "@nestjs/passport";
import {Injectable, UnauthorizedException} from "@nestjs/common";
import {AuthService} from "../auth.service";
import {UnauthorizedAuth} from "../exception/UnauthorizedAuth";
import {AccountNotExists} from "src/account/exceptions/accountNotExists";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({usernameField: "email"});
  }

  async validate(email: string, password: string): Promise<any> {
    try {
      const account = await this.authService.validateUser(email, password);

      if (!account) throw new UnauthorizedAuth();

      return account;
    } catch (error) {
      if (error instanceof UnauthorizedAuth)
        throw new UnauthorizedException(error.message);
      if (error instanceof AccountNotExists)
        throw new UnauthorizedException(error.message);
    }
  }
}
