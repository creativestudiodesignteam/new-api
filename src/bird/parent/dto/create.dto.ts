import {IsNumber} from "class-validator";

export class ParentsCreateDto {
  @IsNumber()
  mother: number;
  @IsNumber()
  father: number;
}
