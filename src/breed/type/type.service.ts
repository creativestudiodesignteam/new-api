import {Injectable} from "@nestjs/common";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {DeepPartial} from "typeorm";
import {BreedTypesNotExists} from "./exception/typeNotExists";
import {BreedTypesAlreadyExists} from "./exception/typesAlreadyExists";
import {BreedsTypesCreateDto} from "./dto/create.dto";
import {BreedsTypeEditDTO} from "./dto/edit.dto";
import {BreedsTypesDTO} from "./dto/types.dto";
import {BreedsTypes} from "./type.entity";
import {BreedTypeRepository} from "./type.repository";

@Injectable()
export class BreedTypeService {
  constructor(private breedTypeRepository: BreedTypeRepository) {}
  async findAll(queryParams: QueryParams) {
    return await this.breedTypeRepository.list(queryParams);
  }

  async findById(id: number): Promise<BreedsTypesDTO> {
    const BreedTypeFinded = await this.breedTypeRepository.findById(id);

    if (!BreedTypeFinded) throw new BreedTypesNotExists();

    return BreedTypeFinded;
  }

  async create(createToBeTypes: BreedsTypesCreateDto): Promise<BreedsTypesDTO> {
    const checkBreedType = await this.breedTypeRepository.findBy({
      name: createToBeTypes.name,
    });

    if (checkBreedType) throw new BreedTypesAlreadyExists();

    const breedsTypes = new BreedsTypes();
    breedsTypes.name = createToBeTypes.name;

    await this.breedTypeRepository.store(breedsTypes);

    return breedsTypes;
  }

  async update(id: number, breedsTypesChanges: DeepPartial<BreedsTypeEditDTO>) {
    const checkBreedsTypes = await this.breedTypeRepository.findBy({
      name: breedsTypesChanges.name,
    });

    if (checkBreedsTypes) throw new BreedTypesAlreadyExists();

    const breedsTypesFinded = await this.breedTypeRepository.findById(id);

    if (!breedsTypesFinded) throw new BreedTypesNotExists();

    const breedsTypes = this.breedTypeRepository.create({
      id,
      ...breedsTypesFinded,
      ...breedsTypesChanges,
    });

    await this.breedTypeRepository.updateBreedsType(breedsTypes);

    return breedsTypes;
  }

  async delete(id: number) {
    const categories = await this.breedTypeRepository.findById(id);

    if (!categories) throw new BreedTypesNotExists();
    this.breedTypeRepository.destroy(categories);
  }
}
