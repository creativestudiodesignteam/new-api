import {CageDTO} from "./cage.dto";

export class CageListDto {
  Cage: CageDTO[];
  page: number;
  perPage: number;

  constructor(cage: CageDTO[], page: number, perPage: number) {
    this.Cage = cage;
    this.page = page;
    this.perPage = perPage;
  }

  public getCageData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      Cage: this.Cage,
    };
  }
}
