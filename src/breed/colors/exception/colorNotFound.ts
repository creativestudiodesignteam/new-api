import {AppError} from "src/shared/error/AppError";

export class ColorNotExists extends AppError {
  constructor() {
    super("Uma das cores selecionadas não existe", 404);
  }
}
