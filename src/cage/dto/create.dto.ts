import {AccountDTO} from "src/account/dto/account.dto";

export class CageCreateDto {
  id: number;
  name: string;
  state?: boolean;
  account: AccountDTO;
}
