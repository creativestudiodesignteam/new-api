import {ValidationPipe} from "./../shared/pipes/validation.pipe";
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Patch,
  Post,
  Put,
  Query,
  Request,
  UseGuards,
} from "@nestjs/common";
import {AccountService} from "./account.service";
import {AccountCreateDto} from "./dto/create.dto";
import {QueryParams} from "../shared/dto/queryParams.dto";

import {ApiTags} from "@nestjs/swagger";

import {AccountDTO} from "./dto/account.dto";
import {AccountListDto} from "./dto/list.dto";
import {AccountEditDto} from "./dto/edit.dto";
import {AccountPasswordEditDTO} from "./dto/editPassword.dto";
import {CustomSuccessResponse} from "src/shared/dto/customResponse.dto";
import {JwtAuthGuard} from "src/auth/jwt/jwt-auth.guard";

@ApiTags("Accounts")
@Controller("account")
export class AccountController {
  constructor(private accountService: AccountService) {}

  @Get("all")
  async findAll(
    @Query(new ValidationPipe()) queryParams: QueryParams,
  ): Promise<any> {
    const AccountCreate = await this.accountService.findAll(queryParams);

    const paginatedList = new AccountListDto(
      AccountCreate,
      queryParams.page ?? 1,
      queryParams.perPage ?? 10,
    );

    return paginatedList;
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async findByUser(@Request() req): Promise<AccountDTO> {
    const AccountCreate = await this.accountService.findById(req.id);
    return AccountCreate;
  }

  @Post()
  async create(@Body(new ValidationPipe()) account: AccountCreateDto) {
    const AccountCreate = await this.accountService.create(account);
    return AccountCreate;
  }

  @UseGuards(JwtAuthGuard)
  @Put()
  async update(
    @Request() req,
    @Body(new ValidationPipe())
    accountEdit: AccountEditDto,
  ): Promise<AccountDTO> {
    const updateAccount = this.accountService.update(req.id, accountEdit);

    return updateAccount;
  }

  @UseGuards(JwtAuthGuard)
  @Patch("password")
  async updatePassword(
    @Request() req,
    @Body(new ValidationPipe())
    accountEdit: AccountPasswordEditDTO,
  ): Promise<AccountDTO> {
    const updateAccountPassword = this.accountService.update_password(
      req.id,
      accountEdit,
    );

    return updateAccountPassword;
  }

  @UseGuards(JwtAuthGuard)
  @Delete()
  @HttpCode(200)
  async delete(@Request() req): Promise<CustomSuccessResponse> {
    await this.accountService.delete(req.id);

    return new CustomSuccessResponse("Conta excluida com sucesso");
  }
}
