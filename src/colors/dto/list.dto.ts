import {ColorsDTO} from "./colors.dto";

export class ColorsListDto {
  Colors: ColorsDTO[];
  page: number;
  perPage: number;

  constructor(colors: ColorsDTO[], page: number, perPage: number) {
    this.Colors = colors;
    this.page = page;
    this.perPage = perPage;
  }

  public getColorsData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      Colors: this.Colors,
    };
  }
}
