import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("cages", table => {
    table.increments("id").primary();
    table.string("name", 80).notNullable();
    table.boolean("state").defaultTo(true).notNullable();
    table
      .integer("account")
      .references("id")
      .inTable("accounts")
      .notNullable();
    table.timestamps(undefined, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("cages");
}