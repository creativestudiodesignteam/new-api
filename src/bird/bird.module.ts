import {Module} from "@nestjs/common";
import {TypeOrmModule} from "@nestjs/typeorm";
import {AccountRepository} from "src/account/account.repository";
import {AccountService} from "src/account/account.service";
import {BreedsRepository} from "src/breed/breed.repository";
import {BreedService} from "src/breed/breed.service";
import {CategoriesRepository} from "src/categories/categories.repository";
import {CategoriesService} from "src/categories/categories.service";
import {ColorsRepository} from "src/colors/colors.repository";
import {ColorsService} from "src/colors/colors.service";
import {StatusRepository} from "src/status/status.repository";
import {StatusService} from "src/status/status.service";
import {BirdController} from "./bird.controller";
import {BirdsRepository} from "./bird.repository";
import {BirdService} from "./bird.service";
import {BreedModule} from "../breed/breed.module";
import {CategoriesModule} from "src/categories/categories.module";
import {ColorsModule} from "src/colors/colors.module";
import {AccountModule} from "src/account/account.module";
import {StatusModule} from "src/status/status.module";
import {BreedTypeService} from "src/breed/type/type.service";
import {BreedsColorsService} from "src/breed/colors/colors.service";
import {BreedTypeRepository} from "src/breed/type/type.repository";
import {BreedsColorsRepository} from "src/breed/colors/colors.repository";
import {ImageModule} from "src/image/image.module";
import {ImageService} from "src/image/image.service";
import {ImagesRepository} from "src/image/image.repository";
import {SharedModule} from "src/shared/shared.module";
import {UploadFilesService} from "src/shared/upload/upload-files.service";
import {GalleryService} from "./gallery/gallery.service";
import {GalleryRepository} from "./gallery/gallery.repository";
import {GalleryController} from "./gallery/gallery.controller";
import {CageModule} from "src/cage/cage.module";
import {CageService} from "src/cage/cage.service";
import {CagesRepository} from "src/cage/cage.repository";
import {ParentController} from "./parent/parent.controller";
import {ParentService} from "./parent/parent.service";
import {ParentsRepository} from "./parent/parent.repository";
import {BabyController} from "./baby/baby.controller";
import {BabyService} from "./baby/baby.service";
import {BabiesRepository} from "./baby/baby.repository";
import {EggController} from "./egg/egg.controller";
import {EggService} from "./egg/egg.service";
import {EggsRepository} from "./egg/egg.repository";
@Module({
  imports: [
    BreedModule,
    CategoriesModule,
    ColorsModule,
    StatusModule,
    AccountModule,
    ImageModule,
    SharedModule,
    CageModule,
    TypeOrmModule.forFeature([
      BirdsRepository,
      CategoriesRepository,
      BreedsRepository,
      ColorsRepository,
      StatusRepository,
      BreedTypeRepository,
      BreedsColorsRepository,
      ImagesRepository,
      GalleryRepository,
      CagesRepository,
      BabiesRepository,
      ParentsRepository,
      EggsRepository,
    ]),
  ],
  controllers: [
    BirdController,
    GalleryController,
    ParentController,
    BabyController,
    EggController,
  ],
  providers: [
    BirdService,
    CategoriesService,
    BreedService,
    ColorsService,
    StatusService,
    BreedTypeService,
    BreedsColorsService,
    ImageService,
    UploadFilesService,
    GalleryService,
    CageService,
    ParentService,
    BabyService,
    EggService,
  ],
})
export class BirdModule {}
