import {AppError} from "src/shared/error/AppError";

export class CageNotExists extends AppError {
  constructor() {
    super("Gaiola inexisteste", 404);
  }
}
