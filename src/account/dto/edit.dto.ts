import {IsEmail, IsMobilePhone, IsOptional, IsString} from "class-validator";

export class AccountEditDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsOptional()
  @IsMobilePhone()
  phone: string;

  @IsOptional()
  @IsEmail()
  email: string;
}
