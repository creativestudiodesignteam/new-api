import {DeepPartial, EntityRepository, Repository} from "typeorm";
import {BreedsColors} from "./colors.entity";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {BreedDTO} from "../dto/breed.dto";
import {AllowedBreedsColorsRelation} from "./interface/AllowedBreedsColorsRelations";

@EntityRepository(BreedsColors)
export class BreedsColorsRepository extends Repository<BreedsColors> {
  async list(queryParams: QueryParams): Promise<any> {
    const colors = await this.find({
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      order: {id: "ASC"},
    });

    return colors;
  }

  async readByBreedWithRelation(
    breed: BreedDTO,
    queryParams: QueryParams,
    relation: AllowedBreedsColorsRelation[],
  ): Promise<any> {
    const colors = await this.find({
      relations: relation,
      where: {breed},
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      order: {id: "ASC"},
    });

    return colors;
  }
  async findById(id: number): Promise<any> {
    return await this.findOne(id);
  }

  async findBy(paramToSearch) {
    return await this.findOne({where: paramToSearch});
  }

  async findByWithRelation(
    paramToSearch,
    relation: AllowedBreedsColorsRelation[],
  ) {
    return await this.find({where: paramToSearch, relations: relation});
  }

  async store(breedsToBeCreated): Promise<any> {
    return await this.insert(breedsToBeCreated);
  }

  async updateBreedsColors(BreedsColors: DeepPartial<any>) {
    return await this.update(BreedsColors.id, BreedsColors);
  }

  async destroy(breeds): Promise<void> {
    await this.remove(breeds);
  }
}
