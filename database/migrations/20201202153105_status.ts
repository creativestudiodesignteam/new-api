import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  /* 
    @param name
    */
  await knex.schema.createTable("status", table => {
    table.increments("id").primary();
    table.string("name", 80).notNullable();
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("status");
}
