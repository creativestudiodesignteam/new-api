import { Birds } from "src/bird/bird.entity";

export class ParentsDTO {
    id: number;
    mother: Birds;
    father: Birds;
  
    constructor(
      id: number,
      mother: Birds,
      father: Birds,
    ) {
      this.id = id;
      this.mother = mother;
      this.father = father;
    }
  }
  