import {AppError} from "src/shared/error/AppError";

export class BreedNotExists extends AppError {
  constructor() {
    super("Raça não existe", 404);
  }
}
