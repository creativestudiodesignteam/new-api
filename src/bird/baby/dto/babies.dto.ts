import {AccountDTO} from "src/account/dto/account.dto";
import {ParentsDTO} from "src/bird/parent/dto/parent.dto";
import {CageDTO} from "src/cage/dto/cage.dto";
import {StatusDTO} from "src/status/dto/status.dto";

export class BabiesDTO {
  id: number;
  birthdate: Date;
  rightRing: String;
  leftRing: String;
  ring_date: Date;
  separete_date: Date;
  account: AccountDTO;
  cage: CageDTO;
  status: StatusDTO;
  parents: ParentsDTO;

  constructor(
    id: number,
    birthdate: Date,
    rightRing: String,
    leftRing: String,
    ring_date: Date,
    separete_date: Date,
    account: AccountDTO,
    cage: CageDTO,
    status: StatusDTO,
    parents: ParentsDTO,
  ) {
    this.id = id;
    this.birthdate = birthdate;
    this.rightRing = rightRing;
    this.leftRing = leftRing;
    this.ring_date = ring_date;
    this.separete_date = separete_date;
    this.account = account;
    this.cage = cage;
    this.status = status;
    this.parents = parents;
  }
}
