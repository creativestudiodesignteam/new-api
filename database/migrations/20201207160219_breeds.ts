import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("breeds", table => {
    table.increments("id").primary();
    table.string("name", 80).notNullable();
    table
      .integer("breed_type")
      .references("id")
      .inTable("breeds_types")
      .notNullable();
      table
      .integer("category")
      .references("id")
      .inTable("categories")
      .notNullable();
    table.timestamps(undefined, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("breeds");
}
