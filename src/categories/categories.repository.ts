import {DeepPartial, EntityRepository, Repository} from "typeorm";
import {Categories} from "./categories.entity";
import {CategoriesDTO} from "./dto/categories.dto";
import {CategoriesCreateDto} from "./dto/create.dto";
import { QueryParams } from "src/shared/dto/queryParams.dto";

@EntityRepository(Categories)
export class CategoriesRepository extends Repository<Categories> {
  async list(queryParams: QueryParams): Promise<CategoriesDTO[]> {
    const categories = await this.find({
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      order: {id: "ASC"},
    });

    return categories;
  }

  async findById(id: number): Promise<Categories> {
    return await this.findOne(id);
  }
  
  async store(categoriesToBeCreated: CategoriesCreateDto): Promise<any> {
    return await this.insert(categoriesToBeCreated);
  }

  async updateCategories(Categories: DeepPartial<CategoriesDTO>) {
    return await this.update(Categories.id, Categories);
  }

  async destroy(Categories: Categories): Promise<void> {
    await this.remove(Categories);
  }
}
