import {DeepPartial, EntityRepository, Repository} from "typeorm";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {AccountDTO} from "src/account/dto/account.dto";
import {Gallery} from "./gallery.entity";
import {GalleryCreateDtoIn} from "./dto/create.dto";
import {AllowedGaleryRelation} from "./interface/AllowRelationsGallery";
/* import {BirdsQueryParams} from "./dto/bird-query-params.dto"; */

@EntityRepository(Gallery)
export class GalleryRepository extends Repository<Gallery> {
  async findAll(queryParams): Promise<Gallery[]> {
    const birds = this.find({
      take: queryParams.perPage,
      skip: queryParams.perPage * (queryParams.page - 1),
    });

    return birds;
  }

  findById(id: number): Promise<Gallery> {
    return this.findOne(id);
  }

  async findBy(paramToSearch) {
    return this.findOne({where: paramToSearch});
  }

  async findByWithRelation(paramToSearch, relation: AllowedGaleryRelation[]) {
    return this.findOne({where: paramToSearch, relations: relation});
  }

  async findAllByWithRelation(
    paramToSearch,
    relation: AllowedGaleryRelation[],
    queryParams: QueryParams,
  ) {
    return await this.find({
      where: paramToSearch,
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      relations: relation,
    });
  }

  store(galleryToBeCreated: any) {
    return this.insert(galleryToBeCreated);
  }

  async updateBirds(birds: DeepPartial<any>) {
    return await this.update(birds.id, birds);
  }

  destroy(birds: Gallery) {
    this.remove(birds);
  }
}
