import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("birds", table => {
    table.increments("id").primary();
    table.string("name", 80).notNullable();
    table.date("birthdate").notNullable();
    table.string("rightRing", 80);
    table.string("leftRing", 80);
    table.string("gender", 80);
    table.string("registry", 80);
    table
      .integer("account")
      .references("id")
      .inTable("accounts")
      .notNullable();
    table
      .integer("category")
      .references("id")
      .inTable("categories")
      .notNullable();
    table
      .integer("breed")
      .references("id")
      .inTable("breeds")
      .notNullable();
    table
      .integer("cages")
      .references("id")
      .inTable("cages")
      .notNullable();
    table
      .integer("color")
      .references("id")
      .inTable("colors")
      .notNullable();
    table
      .integer("status")
      .references("id")
      .inTable("status")
      .notNullable();
    table.timestamps(undefined, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("birds");
}
