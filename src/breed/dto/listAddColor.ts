import {BreedDTO} from "./breed.dto";

export class BreedAddColorDto {
  Breeds: BreedDTO[];
  page: number;
  perPage: number;

  constructor(breeds: BreedDTO[], page: number, perPage: number) {
    this.Breeds = breeds;
    this.page = page;
    this.perPage = perPage;
  }

  public getBreedsData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      Breeds: this.Breeds,
    };
  }
}
