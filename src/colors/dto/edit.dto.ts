import {IsString} from "class-validator";

export class ColorsEditDto {
  @IsString()
  name: string;
}
