import {ValidationPipe} from "../shared/pipes/validation.pipe";
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Query,
  Post,
  Request,
  Put,
} from "@nestjs/common";
import {CategoriesCreateDto} from "./dto/create.dto";

import {CategoriesListDto} from "./dto/list.dto";
import {CustomSuccessResponse} from "src/shared/dto/customResponse.dto";
import {CategoriesService} from "./categories.service";
import {CategoriesDTO} from "./dto/categories.dto";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {CategoriesEditDto} from "./dto/edit.dto";

@Controller("categories")
export class CategoriesController {
  constructor(private categoriesService: CategoriesService) {}

  @Get()
  async findAll(
    @Query(new ValidationPipe()) queryParams: QueryParams,
  ): Promise<any> {
    const CategoriesFindAll = await this.categoriesService.findAll(queryParams);

    const paginatedList = new CategoriesListDto(
      CategoriesFindAll,
      queryParams.page ?? 1,
      queryParams.perPage ?? 10,
    );

    return paginatedList;
  }

  @Get(":id")
  async findById(@Param() param): Promise<CategoriesDTO> {
    const CategoriesFindById = await this.categoriesService.findById(param.id);
    return CategoriesFindById;
  }

  @Post()
  async create(@Body(new ValidationPipe()) categories: CategoriesCreateDto) {
    const CategoriesCreate = await this.categoriesService.create(categories);
    return CategoriesCreate;
  }

  @Put()
  async update(
    @Request() req,
    @Body(new ValidationPipe())
    categoriesEdit: CategoriesEditDto,
  ): Promise<CategoriesDTO> {
    const updateCategories = this.categoriesService.update(
      req.id,
      categoriesEdit,
    );

    return updateCategories;
  }

  @Delete()
  @HttpCode(200)
  async delete(@Param() param): Promise<CustomSuccessResponse> {
    await this.categoriesService.delete(param.id);

    return new CustomSuccessResponse("Categories excluido com sucesso");
  }
}
