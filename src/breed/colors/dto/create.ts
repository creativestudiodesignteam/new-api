import {Breeds} from "src/breed/breed.entity";
import {ColorsDTO} from "src/colors/dto/colors.dto";
interface colors {
  id: number;
}

export class BreedsColorsCreateDto {
  breed: number;
  colors: colors[];
}
