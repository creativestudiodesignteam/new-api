import {CategoriesDTO} from "./categories.dto";

export class CategoriesListDto {
  Categories: CategoriesDTO[];
  page: number;
  perPage: number;

  constructor(categories: CategoriesDTO[], page: number, perPage: number) {
    this.Categories = categories;
    this.page = page;
    this.perPage = perPage;
  }

  public getCategoriesData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      Categories: this.Categories,
    };
  }
}
