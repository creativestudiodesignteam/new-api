import {Birds} from "./bird.entity";
import {DeepPartial, EntityRepository, Repository} from "typeorm";
import {BirdsCreateDtoOut} from "./dto/create-out.dto";
import {AllowedBirdsRelation} from "./interface/allowedBirdsRelations";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {BirdsDTO} from "./dto/birds.dto";
import {QueryParamsWithFilter} from "./dto/queryParamsWithFilter.dto";
import {AccountDTO} from "src/account/dto/account.dto";
/* import {BirdsQueryParams} from "./dto/bird-query-params.dto"; */

@EntityRepository(Birds)
export class BirdsRepository extends Repository<Birds> {
  async findAll(queryParams): Promise<Birds[]> {
    const birds = this.find({
      take: queryParams.perPage,
      skip: queryParams.perPage * (queryParams.page - 1),
    });

    return birds;
  }

  findById(id: number): Promise<Birds> {
    return this.findOne(id);
  }

  async findBy(paramToSearch) {
    return this.findOne({where: paramToSearch});
  }

  async findByWithRelation(paramToSearch, relation: AllowedBirdsRelation[]) {
    return this.findOne({where: paramToSearch, relations: relation});
  }

  async findAllByWithRelation(
    paramToSearch,
    relation: AllowedBirdsRelation[],
    queryParams: QueryParamsWithFilter,
  ) {
    return await this.find({
      where: paramToSearch,
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      relations: relation,
    });
  }

  async findCountAll(account: AccountDTO): Promise<number> {
    return await this.count({
      where: {account},
    });
  }

  store(birdToBeCreated: BirdsCreateDtoOut) {
    return this.insert(birdToBeCreated);
  }

  async updateBirds(birds: DeepPartial<any>) {
    return await this.update(birds.id, birds);
  }

  destroy(birds: Birds) {
    this.remove(birds);
  }
}
