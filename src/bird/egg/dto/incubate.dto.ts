import {IsNumber} from "class-validator";

export class IncubateDTO {
  id: number;
  @IsNumber()
  birthdate: number;
  @IsNumber()
  cadlingDate: number;
}
