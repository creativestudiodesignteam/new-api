import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  /* 
    @params cpf(NullLabel), name,phone,email,password
    */
  await knex.schema.createTable("accounts", table => {
    table.increments("id").primary();
    table.string("cpf", 80);
    table.string("name", 80).notNullable();
    table.string("phone", 80).notNullable();
    table.string("email", 80).notNullable();
    table.string("password", 80).notNullable();
    table.timestamps(undefined, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("accounts");
}
