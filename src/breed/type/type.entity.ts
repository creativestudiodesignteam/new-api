import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Breeds} from "../breed.entity";
@Entity({name: "breeds_types"})
export class BreedsTypes {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(
    () => Breeds,
    breeds => breeds.breed_type,
  )
  breeds: Breeds;
}
