import {IsDate, IsNumber, IsNumberString, IsOptional} from "class-validator";
export class BabiesCreateDto {
  id: number;
  @IsOptional()
  birthdate: Date;
  @IsNumber()
  cage: number;
  @IsNumber()
  status: number;
  @IsOptional()
  @IsNumber()
  mother: number;
  @IsOptional()
  @IsNumber()
  father: number;
}
