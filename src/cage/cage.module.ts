import {Module} from "@nestjs/common";
import {CageService} from "./cage.service";
import {CageController} from "./cage.controller";
import {CagesRepository} from "./cage.repository";
import {TypeOrmModule} from "@nestjs/typeorm";
import { AccountModule } from "src/account/account.module";

@Module({
  imports: [TypeOrmModule.forFeature([CagesRepository]), AccountModule],
  providers: [CageService],
  controllers: [CageController],
})
export class CageModule {}
