import {Images} from "../image.entity";

export class ImagesListDto {
  constructor(images: Images) {
    const accountWIthLinks = {
      ...images,
      url: `${process.env.URL_SERVER}/files/read/${images.name}`,
    };

    return accountWIthLinks;
  }
}
