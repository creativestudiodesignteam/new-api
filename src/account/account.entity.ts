import {Cage} from "src/cage/cage.entity";
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import {Birds} from "../bird/bird.entity";

import {Babies} from "../bird/baby/baby.entity";

@Entity({name: "accounts"})
export class Account {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  cpf: string;

  @Column()
  name: string;

  @Column()
  phone: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @OneToMany(
    () => Birds,
    bird => bird.account,
  )
  birds: Birds;

  @OneToMany(
    () => Cage,
    cages => cages.account,
  )
  cages: Cage;

  @OneToMany(
    () => Babies,
    babies => babies.account,
  )
  babies: Babies;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
