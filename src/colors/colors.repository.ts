import {DeepPartial, EntityRepository, Repository} from "typeorm";
import {Colors} from "./colors.entity";
import {ColorsDTO} from "./dto/colors.dto";
import {ColorsCreateDto} from "./dto/create.dto";
import { QueryParams } from "src/shared/dto/queryParams.dto";

@EntityRepository(Colors)
export class ColorsRepository extends Repository<Colors> {
  async list(queryParams: QueryParams): Promise<ColorsDTO[]> {
    const colors = await this.find({
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      order: {id: "ASC"},
    });

    return colors;
  }

  async findById(id: number): Promise<Colors> {
    return await this.findOne(id);
  }
  
  async store(colorsToBeCreated: ColorsCreateDto): Promise<any> {
    return await this.insert(colorsToBeCreated);
  }

  async updateColors(Colors: DeepPartial<ColorsDTO>) {
    return await this.update(Colors.id, Colors);
  }

  async destroy(Colors: Colors): Promise<void> {
    await this.remove(Colors);
  }
}
