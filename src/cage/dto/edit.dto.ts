import {IsOptional, IsString} from "class-validator";
import { AccountDTO } from "src/account/dto/account.dto";

export class CageEditDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  state?: boolean;
  
  @IsOptional()
  account: AccountDTO;
}
