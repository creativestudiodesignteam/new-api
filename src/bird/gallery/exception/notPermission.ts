import {AppError} from "src/shared/error/AppError";

export class NotPermission extends AppError {
  constructor() {
    super("Você não tem permissão para executar esta função", 401);
  }
}
