import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("birds_parents", table => {
    table.increments("id").primary();
    table
      .integer("mother")
      .references("id")
      .inTable("birds")
      .onDelete("CASCADE")
      .onUpdate("CASCADE")
      .notNullable();
    table
      .integer("father")
      .references("id")
      .inTable("birds")
      .onDelete("CASCADE")
      .onUpdate("CASCADE")
      .notNullable();
    table.timestamps(undefined, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("birds_parents");
}
