import {IsString} from "class-validator";

export class BreedsTypesCreateDto {
  @IsString()
  name: string;
}
