import {BirdsDTO} from "src/bird/dto/birds.dto";
import {BirdsToListDTO} from "src/bird/dto/birdsToList.dto";
import {CategoriesDTO} from "src/categories/dto/categories.dto";
import {ColorsDTO} from "src/colors/dto/colors.dto";
import {ImageWithLink} from "src/image/dto/imageWithLink";
import {Images} from "src/image/image.entity";

export class GalleryDTO {
  id: number;
  image: ImageWithLink;
  bird: BirdsToListDTO;

  constructor(id: number, image: ImageWithLink, bird: BirdsToListDTO) {
    this.id = id;
    this.image = image;
    this.bird = bird;
  }
}
