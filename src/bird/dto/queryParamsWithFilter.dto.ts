import {IsNumberString, IsOptional, IsString} from "class-validator";
export class QueryParamsWithFilter {
  @IsNumberString()
  @IsOptional()
  perPage: number;

  @IsNumberString()
  @IsOptional()
  page: number;

  @IsNumberString()
  @IsOptional()
  birthdate: Date;

  @IsString()
  @IsOptional()
  gender: "Macho" | "Fêmea";

  @IsNumberString()
  @IsOptional()
  color: number;

  @IsNumberString()
  @IsOptional()
  category: number;

  @IsNumberString()
  @IsOptional()
  breeds: number;

  @IsNumberString()
  @IsOptional()
  status: number;
}
