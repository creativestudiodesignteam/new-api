import {AppError} from "src/shared/error/AppError";

export class PasswordNotMatch extends AppError {
  constructor() {
    super("Senha antiga inválida", 400);
  }
}
