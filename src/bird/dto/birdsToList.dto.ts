import {Account} from "src/account/account.entity";
import {BreedDTO} from "src/breed/dto/breed.dto";
import {CageDTO} from "src/cage/dto/cage.dto";
import {CategoriesDTO} from "src/categories/dto/categories.dto";
import {ColorsDTO} from "src/colors/dto/colors.dto";
import {ImagesListDto} from "src/image/dto/list.dto";
import {StatusDTO} from "src/status/dto/status.dto";

export class BirdsToListDTO {
  id: number;
  name: string;
  birthdate: Date;
  rightRing: string;
  leftRing: string;
  gender: string;
  registry: string;
  category: CategoriesDTO;
  breeds: BreedDTO;
  color: ColorsDTO;
  status: StatusDTO;
  account: Account;
  cage: CageDTO;

  constructor(
    id: number,
    name: string,
    birthdate: Date,
    rightRing: string,
    leftRing: string,
    gender: string,
    registry: string,
    category: CategoriesDTO,
    breeds: BreedDTO,
    color: ColorsDTO,
    status: StatusDTO,
    account: Account,
    cage: CageDTO,
  ) {
    this.id = id;
    this.name = name;
    this.birthdate = birthdate;
    this.rightRing = rightRing;
    this.leftRing = leftRing;
    this.gender = gender;
    this.registry = registry;
    this.category = category;
    this.breeds = breeds;
    this.color = color;
    this.status = status;
    this.account = account;
    this.cage = cage;
  }
}
