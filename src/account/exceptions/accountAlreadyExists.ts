import {AppError} from "src/shared/error/AppError";

export class AccountAlreadyExists extends AppError {
  constructor() {
    super("Esta conta já existe, tente novamente", 400);
  }
}
