import {
  Body,
  Controller,
  Post,
  UseGuards,
  Request,
  UseInterceptors,
  UploadedFiles,
  UploadedFile,
  Param,
  Get,
  Query,
  Delete,
  HttpCode,
} from "@nestjs/common";
import {FileFieldsInterceptor, FileInterceptor} from "@nestjs/platform-express";
import {id} from "date-fns/locale";
import {diskStorage} from "multer";
import {JwtAuthGuard} from "src/auth/jwt/jwt-auth.guard";
import {CustomSuccessResponse} from "src/shared/dto/customResponse.dto";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {ValidationPipe} from "src/shared/pipes/validation.pipe";
import {editFileName} from "src/shared/upload/utils/upload-files";
import {GalleryListDto} from "./dto/list.dto";
import {GalleryService} from "./gallery.service";

@Controller("gallery")
export class GalleryController {
  constructor(private galeryService: GalleryService) {}

  @UseGuards(JwtAuthGuard)
  @Get("read/:id_birds")
  async findAll(
    @Request() req,
    @Param() param,
    @Query(new ValidationPipe()) queryParams: QueryParams,
  ): Promise<any> {
    const galleryFindAll = await this.galeryService.findAll(
      param.id_birds,
      queryParams,
    );

    const paginatedList = new GalleryListDto(
      galleryFindAll,
      queryParams.page ?? 1,
      queryParams.perPage ?? 10,
    );

    return paginatedList;
  }

  @UseGuards(JwtAuthGuard)
  @Post("/:bird")
  @UseInterceptors(
    FileFieldsInterceptor([{name: "files"}], {
      fileFilter: (req, file, cb) => {
        if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
          req.fileErrors = {
            error: true,
          };
          return cb(null, false);
        }
        return cb(null, true);
      },
      storage: diskStorage({
        destination: "./upload/gallery",
        filename: editFileName,
      }),
    }),
  )
  async create(@UploadedFiles() files, @Request() req, @Param() param) {
    const createGallery = await this.galeryService.create(
      req.user.id,
      param.bird,
      files,
    );

    return createGallery;
  }

  @UseGuards(JwtAuthGuard)
  @Delete(":id")
  @HttpCode(200)
  async delete(@Param() param, @Request() req): Promise<CustomSuccessResponse> {
    await this.galeryService.delete(req.id, param.id);

    return new CustomSuccessResponse("Imagem da galeria excluido com sucesso");
  }
}
