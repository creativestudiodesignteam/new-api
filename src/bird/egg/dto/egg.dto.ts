import {AccountDTO} from "src/account/dto/account.dto";
import {ParentsDTO} from "src/bird/parent/dto/parent.dto";
import {CageDTO} from "src/cage/dto/cage.dto";

export class EggsDTO {
  id: number;
  birthdate: Date;
  layingDate: Date;
  hatchingDate: Date;
  candlingDate: Date;
  state: String;
  account: AccountDTO;
  cage: CageDTO;
  parents: ParentsDTO;
  isShocked: Boolean;

  constructor(
    id: number,
    birthdate: Date,
    layingDate: Date,
    hatchingDate: Date,
    candlingDate: Date,
    isShocked: Boolean,
    state: String,
    account: AccountDTO,
    cage: CageDTO,
    parents: ParentsDTO,
  ) {
    this.id = id;
    this.birthdate = birthdate;
    this.layingDate = layingDate;
    this.hatchingDate = hatchingDate;
    this.candlingDate = candlingDate;
    this.isShocked = isShocked;
    this.state = state;
    this.account = account;
    this.cage = cage;
    this.parents = parents;
  }
}
