import {Module} from "@nestjs/common";
import {TypeOrmModule} from "@nestjs/typeorm";
import {EncryptedService} from "src/shared/encrypted/encrypted.service";
import {AccountController} from "./account.controller";
import {AccountRepository} from "./account.repository";
import {AccountService} from "./account.service";

@Module({
  imports: [TypeOrmModule.forFeature([AccountRepository]), EncryptedService],
  controllers: [AccountController],
  providers: [AccountService, EncryptedService],
  exports: [AccountService],
})
export class AccountModule {}
