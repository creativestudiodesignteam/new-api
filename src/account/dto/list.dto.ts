import {PaginatedList} from "./../../shared/interface/paginated";
import {AccountDTO} from "./account.dto";

export class AccountListDto implements PaginatedList {
  Accounts: AccountDTO[];
  page: number;
  perPage: number;

  constructor(accounts: AccountDTO[], page: number, perPage: number) {
    /* Deleting passwords to not be shown in the request*/
    accounts.map(account => {
      delete account.password;
      return account;
    });

    this.Accounts = accounts;
    this.page = page;
    this.perPage = perPage;
  }

  public getPaginatedData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      Accounts: this.Accounts,
    };
  }
}
