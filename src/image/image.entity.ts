import {Birds} from "src/bird/bird.entity";
import {Gallery} from "src/bird/gallery/gallery.entity";
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity({name: "images"})
export class Images {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  path: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToMany(
    () => Gallery,
    gallery => gallery.image,
  )
  gallery: Gallery;
}
