import {IsEmail, IsString, IsMobilePhone} from "class-validator";
export class AccountCreateDto {
  cpf?: string;
  name: string;
  @IsMobilePhone()
  phone: string;
  @IsEmail()
  email: string;
  @IsString()
  password: string;
}
