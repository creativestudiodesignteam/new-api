import {AppError} from "src/shared/error/AppError";

export class RelationNotFound extends AppError {
  constructor() {
    super("Relação entre cor e raça inexistente", 404);
  }
}
