import {EncryptedService} from "src/shared/encrypted/encrypted.service";
import {AccountService} from "src/account/account.service";
import {Injectable} from "@nestjs/common";
import {JwtService} from "@nestjs/jwt";
import {UnauthorizedAuth} from "./exception/UnauthorizedAuth";
import {AccountNotExists} from "src/account/exceptions/accountNotExists";
@Injectable()
export class AuthService {
  constructor(
    private accountService: AccountService,
    private encryptService: EncryptedService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    try {
      const account = await this.accountService.findByEmail(email);
      
      const comparePasswords = await this.encryptService.compare(
        password,
        account.password,
      );

      if (!account || !comparePasswords) throw new UnauthorizedAuth();

      const {password: password_account, ...result} = account;

      return result;
    } catch (error) {
      if (error instanceof AccountNotExists) throw new UnauthorizedAuth();
    }
  }

  async login(account: any) {
    const payload = {id: account.id, name: account.name, email: account.email};

    return {
      account: payload,
      access_token: this.jwtService.sign(payload),
    };
  }
}
