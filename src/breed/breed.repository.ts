import {DeepPartial, EntityRepository, Repository} from "typeorm";
import {Breeds} from "./breed.entity";
import {BreedDTO} from "./dto/breed.dto";
import {BreedsCreateDto} from "./dto/create.dto";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {AllowedBreedsRelation} from "./interface/AllowedBreedsRelations";

@EntityRepository(Breeds)
export class BreedsRepository extends Repository<Breeds> {
  async list(queryParams: QueryParams): Promise<BreedDTO[]> {
    const colors = await this.find({
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      order: {name: "ASC"},
    });

    return colors;
  }

  async findById(id: number): Promise<BreedDTO> {
    return await this.findOne(id);
  }

  async findByWithRelation(
    paramToSearch,
    queryParams: QueryParams,
    relation: AllowedBreedsRelation[],
  ) {
    return await this.find({
      where: paramToSearch,
      relations: relation,
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      order: {name: "ASC"},
    });
  }

  async findWithRelation(id: number, relation: AllowedBreedsRelation[]) {
    return await this.findOne({relations: relation, where: {id}});
  }

  async findBy(paramToSearch) {
    return await this.findOne({where: paramToSearch});
  }

  async store(breedsToBeCreated: BreedsCreateDto): Promise<any> {
    return await this.insert(breedsToBeCreated);
  }

  async updateBreeds(breeds: DeepPartial<BreedDTO>) {
    return await this.update(breeds.id, breeds);
  }

  async destroy(breeds: Breeds): Promise<void> {
    await this.remove(breeds);
  }
}
