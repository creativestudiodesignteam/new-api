import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
  Query,
  Request,
  UseGuards,
  ValidationPipe,
} from "@nestjs/common";
import {JwtAuthGuard} from "src/auth/jwt/jwt-auth.guard";
import {CustomSuccessResponse} from "src/shared/dto/customResponse.dto";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import { Cage } from "./cage.entity";
import {CageService} from "./cage.service";
import {CageDTO} from "./dto/cage.dto";
import {CageCreateDto} from "./dto/create.dto";
import {CageEditDto} from "./dto/edit.dto";
import {CageListDto} from "./dto/list.dto";

@Controller("cages")
export class CageController {
  constructor(private cagesService: CageService) {}

  @UseGuards(JwtAuthGuard)
  @Get("read")
  async findAll(
    @Request() req,
    @Query(new ValidationPipe()) queryParams: QueryParams,
  ) {
    const findedCages = await this.cagesService.findByUser(req.id, queryParams);

    const paginatedList = new CageListDto(
      findedCages,
      queryParams.page ?? 1,
      queryParams.perPage ?? 10,
    );

    return paginatedList;
  }

  @UseGuards(JwtAuthGuard)
  @Get(":id")
  async findById(@Param() param): Promise<CageDTO> {
    const CageFindById = await this.cagesService.findById(
      param.id,
      param.id_account,
    );
    return CageFindById;
  }

  @UseGuards(JwtAuthGuard)
  @Post(":id")
  async create(@Param() param) {
    const CagesCreate = await this.cagesService.create(param.id_account);
    return CagesCreate;
  }

  @UseGuards(JwtAuthGuard)
  @Put(":id")
  async update(
    @Param() param,
    @Request() req,
    @Body(new ValidationPipe())
    cageEdit: CageEditDto,
  ): Promise<Cage> {
    const updateCages = this.cagesService.update(param.id, req.id, cageEdit);

    return updateCages;
  }

  @UseGuards(JwtAuthGuard)
  @Delete()
  @HttpCode(200)
  async delete(@Param() param): Promise<CustomSuccessResponse> {
    await this.cagesService.delete(param.id);

    return new CustomSuccessResponse("Categories excluido com sucesso");
  }
}
