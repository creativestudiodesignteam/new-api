export type AllowedParentsRelation =
  | "father"
  | "mother"
