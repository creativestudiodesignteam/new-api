import {Babies} from "src/bird/baby/baby.entity";
import {Birds} from "src/bird/bird.entity";
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

import {Account} from "../account/account.entity";

@Entity({name: "cages"})
export class Cage {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  state: boolean;

  @ManyToOne(
    () => Account,
    accounts => accounts.cages,
  )
  @JoinColumn({name: "account"})
  account: Account;

  @OneToMany(
    () => Birds,
    birds => birds.cage,
  )
  bird: Birds;

  @OneToMany(
    () => Babies,
    babies => babies.cage,
  )
  babies: Babies;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
