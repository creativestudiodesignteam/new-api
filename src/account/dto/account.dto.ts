export class AccountDTO {
  id: number;
  name: string;
  cpf?: string;
  email: string;
  phone: string;
  password: string;
  created_at?: Date;
  updated_at?: Date;

  constructor(
    id: number,
    name: string,
    email: string,
    phone: string,
    password: string,
    cpf?: string,
    created_at?: Date,
    updated_at?: Date,
  ) {
    this.id = id;
    this.name = name;
    this.cpf = cpf ?? null;
    this.email = email;
    this.phone = phone;
    this.password = password;
    this.created_at = created_at ?? undefined;
    this.updated_at = updated_at ?? undefined;
  }
}
