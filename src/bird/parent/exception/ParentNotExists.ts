import {AppError} from "src/shared/error/AppError";

export class ParentNotExists extends AppError {
  constructor() {
    super("Parentes inexistentes", 400);
  }
}
