import {PaginatedList} from "src/shared/interface/paginated";
import {BabiesDTO} from "./babies.dto";

export class BabiesListDto implements PaginatedList {
  babies: BabiesDTO[];
  page: number;
  perPage: number;

  constructor(babies: BabiesDTO[], page: number, perPage: number) {
    this.babies = babies;
    this.page = page;
    this.perPage = perPage;
  }

  public getPaginatedData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      Babies: this.babies,
    };
  }
}
