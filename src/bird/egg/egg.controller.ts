import {
  Controller,
  Post,
  UseGuards,
  Request,
  Param,
  Get,
  Query,
  Delete,
  HttpCode,
  Patch,
  Body,
  Req,
} from "@nestjs/common";
import {JwtAuthGuard} from "src/auth/jwt/jwt-auth.guard";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {ValidationPipe} from "src/shared/pipes/validation.pipe";
import {EggService} from "./egg.service";
import {EggsListDto} from "./dto/list.dto";
import {CustomSuccessResponse} from "src/shared/dto/customResponse.dto";
import {IncubateDTO} from "./dto/incubate.dto";
import {BabiesCreateDto} from "../baby/dto/create.dto";

@Controller("egg")
export class EggController {
  constructor(private eggService: EggService) {}

  @UseGuards(JwtAuthGuard)
  @Get("read/all")
  async findAll(
    @Request() req,
    @Query(new ValidationPipe()) queryParams: QueryParams,
  ): Promise<EggsListDto> {
    const eggsFindAll = await this.eggService.findAll(req.user.id, queryParams);

    const paginatedList = new EggsListDto(
      eggsFindAll,
      queryParams.page ?? 1,
      queryParams.perPage ?? 10,
    );

    return paginatedList;
  }

  @UseGuards(JwtAuthGuard)
  @Get("read/by/cages/:id")
  async findByCages(
    @Request() req,
    @Param() param,
    @Query(new ValidationPipe()) queryParams: QueryParams,
  ): Promise<EggsListDto> {
    const eggsFindAll = await this.eggService.findByCage(param.id, queryParams);

    const paginatedList = new EggsListDto(
      eggsFindAll,
      queryParams.page ?? 1,
      queryParams.perPage ?? 10,
    );

    return paginatedList;
  }

  @UseGuards(JwtAuthGuard)
  @Get("read/by/id/:id")
  async findById(@Request() req, @Param() param): Promise<any> {
    const eggsFindById = await this.eggService.findById(param.id);

    return eggsFindById;
  }

  @Post("create/:cage")
  @UseGuards(JwtAuthGuard)
  async create(@Request() req, @Param() param) {
    const createdEgg = await this.eggService.create(req.user.id, param.cage);

    return createdEgg;
  }

  @Patch("incubate/:id")
  @UseGuards(JwtAuthGuard)
  async incubated(
    @Body(new ValidationPipe()) incubateToUpdate: IncubateDTO,
    @Param() param,
    @Req() req,
  ) {
    const createdEgg = await this.eggService.incubate(
      req.user.id,
      param.id,
      incubateToUpdate,
    );

    return createdEgg;
  }

  @Post("hatch/:id")
  @UseGuards(JwtAuthGuard)
  async hatch(
    @Param() param,
    @Req() req,
  ) {
    const hatchEgg = await this.eggService.hatch(req.user.id, param.id);

    return hatchEgg;
  }

  @Patch("fertilize/:id")
  @UseGuards(JwtAuthGuard)
  async fertlize(@Param() param, @Req() req) {
    const fertilizeEgg = await this.eggService.fertilize(req.user.id, param.id);

    return fertilizeEgg;
  }

  @Patch("add/parents/:id")
  @UseGuards(JwtAuthGuard)
  async addParents(@Body() paretsToBeAdd, @Param() param) {
    const createdEgg = await this.eggService.addParents(
      param.id,
      paretsToBeAdd,
    );

    return createdEgg;
  }

  @UseGuards(JwtAuthGuard)
  @Delete(":id")
  @HttpCode(200)
  async delete(@Param() param): Promise<CustomSuccessResponse> {
    await this.eggService.delete(param.id);

    return new CustomSuccessResponse("Ovo excluido com sucesso");
  }
}
