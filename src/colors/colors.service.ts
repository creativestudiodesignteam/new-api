import {Injectable} from "@nestjs/common";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {ColorsCreateDto} from "./dto/create.dto";
import {ColorsDTO} from "./dto/colors.dto";
import {ColorsNotExists} from "./exceptions/colorsNotExists";
import {Colors} from "./colors.entity";
import {ColorsRepository} from "./colors.repository";
import { ColorsEditDto } from "./dto/edit.dto";
import { DeepPartial } from "typeorm";

@Injectable()
export class ColorsService {
  constructor(private colorsRepository: ColorsRepository) {}

  async findAll(queryParams: QueryParams) {
    return await this.colorsRepository.list(queryParams);
  }

  async findById(id: number): Promise<ColorsDTO> {
    const ColorsFinded = await this.colorsRepository.findById(id);

    if (!ColorsFinded) throw new ColorsNotExists();

    return ColorsFinded;
  }

  async create(colorsToBeCreated: ColorsCreateDto): Promise<ColorsCreateDto> {
    const colors = new Colors();
    colors.name = colorsToBeCreated.name;

    await this.colorsRepository.store(colors);

    return colors;
  }

  async update(id: number, colorsChanges: DeepPartial<ColorsEditDto>) {
    const colorsFinded = await this.colorsRepository.findById(id);

    if (!colorsFinded) throw new ColorsNotExists();

    const colors = this.colorsRepository.create({
      id,
      ...colorsFinded,
      ...colorsChanges,
    });

    await this.colorsRepository.updateColors(colors);

    return colors;
  }

  async delete(id: number) {
    const colors = await this.colorsRepository.findById(id);

    if (!colors) throw new ColorsNotExists();
    this.colorsRepository.destroy(colors);
  }
}
