import {Account} from "src/account/account.entity";
import {Status} from "src/status/status.entity";
import {Cage} from "src/cage/cage.entity";

import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import {Parent} from "../parent/parent.entity";
import {ParentsDTO} from "../parent/dto/parent.dto";
import {AccountDTO} from "src/account/dto/account.dto";
import {CageDTO} from "src/cage/dto/cage.dto";
import {StatusDTO} from "src/status/dto/status.dto";

@Entity({name: "birds_eggs"})
export class Eggs {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  hatchingDate: Date;

  @Column()
  candlingDate: Date;

  @Column()
  layingDate: Date;

  @Column()
  birthdate: Date;

  @Column()
  state: String;

  @Column()
  isShocked: Boolean;


  @ManyToOne(
    () => Account,
    accounts => accounts.babies,
  )
  @JoinColumn({name: "account"})
  account: AccountDTO;

  @ManyToOne(
    () => Cage,
    cages => cages.babies,
  )
  @JoinColumn({name: "cages"})
  cage: CageDTO;

  @ManyToOne(
    () => Parent,
    parent => parent.babies,
  )
  @JoinColumn({name: "parents"})
  parents: ParentsDTO;
}
