import {IsNumber, IsString} from "class-validator";
import {BreedsTypes} from "../type/type.entity";

export class BreedEditDTO {
  @IsString()
  name: string;
  @IsNumber()
  breed_type: number;
  @IsNumber()
  category: number;
}
