interface colors {
  id: number;
}

export class InCreateBreed {
  name: string;
  breed_type: number;
  category: number;
  colors: colors[];
}
