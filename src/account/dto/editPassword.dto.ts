import {IsOptional, IsString} from "class-validator";

export class AccountPasswordEditDTO {
  @IsString()
  password: string;

  @IsOptional()
  password_old: string;
}
