import {Injectable} from "@nestjs/common";
import {Account} from "./account.entity";
import {AccountRepository} from "./account.repository";
import {AccountCreateDto} from "./dto/create.dto";
import {cpf} from "cpf-cnpj-validator";
import {CpfInválid} from "./exceptions/cpfInvalid";
import {AccountAlreadyExists} from "./exceptions/accountAlreadyExists";
import {EncryptedService} from "src/shared/encrypted/encrypted.service";
import {AccountNotExists} from "./exceptions/accountNotExists";
import {AccountDTO} from "./dto/account.dto";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {AccountEditDto} from "./dto/edit.dto";
import {DeepPartial} from "typeorm";
import {AccountPasswordEditDTO} from "./dto/editPassword.dto";
import {PasswordNotMatch} from "./exceptions/passwordNotMatch";

@Injectable()
export class AccountService {
  constructor(
    private accountRepository: AccountRepository,
    private encryptedService: EncryptedService,
  ) {}

  async findAll(queryParams: QueryParams) {
    return await this.accountRepository.findAll(queryParams);
  }

  async findByEmail(email: string) {
    return await this.accountRepository.findBy({
      email: email,
    });
  }

  async findById(id: number): Promise<AccountDTO> {
    const AccountFinded = await this.accountRepository.findById(id);

    if (!AccountFinded) throw new AccountNotExists();

    delete AccountFinded.password;

    return AccountFinded;
  }

  async create(
    accountToBeCreated: AccountCreateDto,
  ): Promise<AccountCreateDto> {
    if (accountToBeCreated.cpf) {
      if (!cpf.isValid(accountToBeCreated.cpf)) throw new CpfInválid();

      accountToBeCreated.cpf = cpf.format(accountToBeCreated.cpf);
    }

    accountToBeCreated.password = await this.encryptedService.encrypt(
      accountToBeCreated.password,
    );

    accountToBeCreated.email = accountToBeCreated.email.toLowerCase();

    const checkAccountToEmail = await this.accountRepository.findBy({
      email: accountToBeCreated.email,
    });

    if (checkAccountToEmail) throw new AccountAlreadyExists();

    const account = new Account();
    account.cpf = accountToBeCreated.cpf ?? null;
    account.name = accountToBeCreated.name;
    account.email = accountToBeCreated.email;
    account.phone = accountToBeCreated.phone;
    account.password = accountToBeCreated.password;

    await this.accountRepository.store(account);

    delete account.password;
    return account;
  }

  async update(id: number, accountChanges: DeepPartial<AccountEditDto>) {
    const accountFinded = await this.accountRepository.findById(id);

    if (!accountFinded) throw new AccountNotExists();

    const checkAccountToEmail = await this.accountRepository.findBy({
      email: accountChanges.email,
    });

    if (checkAccountToEmail) throw new AccountAlreadyExists();

    const account = this.accountRepository.create({
      id,
      ...accountFinded,
      ...accountChanges,
    });

    await this.accountRepository.updateAccount(account);

    delete account.password;

    return account;
  }

  async update_password(
    id: number,
    userChanges: DeepPartial<AccountPasswordEditDTO>,
  ) {
    const accountFinded = await this.accountRepository.findById(id);

    if (!accountFinded) throw new AccountNotExists();

    const compare = await this.encryptedService.compare(
      userChanges.password_old,
      accountFinded.password,
    );
    if (!compare) {
      throw new PasswordNotMatch();
    }

    const password_hash = await this.encryptedService.encrypt(
      userChanges.password,
    );

    const account = this.accountRepository.create({
      id,
      ...accountFinded,
      password: password_hash,
    });

    await this.accountRepository.updateAccount(account);

    delete account.password;

    return account;
  }

  async delete(id: number) {
    const account = await this.accountRepository.findById(id);

    if (!account) throw new AccountNotExists();
    this.accountRepository.destroy(account);
  }
}
