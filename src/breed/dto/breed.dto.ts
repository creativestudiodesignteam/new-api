import {CategoriesDTO} from "src/categories/dto/categories.dto";
import {ColorsDTO} from "src/colors/dto/colors.dto";
import {BreedsTypesDTO} from "../type/dto/types.dto";

export class BreedDTO {
  id: number;
  name: string;
  breed_type: BreedsTypesDTO;
  category: CategoriesDTO;
  colors?: ColorsDTO[];

  constructor(
    id: number,
    name: string,
    breed_type: BreedsTypesDTO,
    category: CategoriesDTO,
    colors?: ColorsDTO[],
  ) {
    this.id = id;
    this.name = name;
    this.breed_type = breed_type;
    this.category = category;
    this.colors = colors ?? null;
  }
}
