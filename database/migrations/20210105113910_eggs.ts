import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("birds_eggs", table => {
    table.increments("id").primary();
    table.date("birthdate");
    table.date("layingDate").notNullable();
    table.date("hatchingDate");
    table.date("candlingDate");
    table.string("state", 80);
    table.boolean("isShocked").defaultTo(false);
    table
      .integer("account")
      .references("id")
      .inTable("accounts")
      .notNullable()
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("cages")
      .references("id")
      .inTable("cages")
      .notNullable()
      .onDelete("SET NULL")
      .onUpdate("CASCADE");
    table
      .integer("parents")
      .references("id")
      .inTable("birds_parents")
      .onDelete("SET NULL")
      .onUpdate("CASCADE");

    table.timestamps(undefined, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("birds_eggs");
}
