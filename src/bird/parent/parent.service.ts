import {forwardRef, Inject, Injectable} from "@nestjs/common";
import {ParentsRepository} from "./parent.repository";
import {ParentsCreateDto} from "./dto/create.dto";
import {BirdService} from "../bird.service";
import {FatherGenderInválid} from "./exception/FatherGenderInvalid";
import {Parent} from "./parent.entity";
import {ChildrenNotExists} from "./exception/ChildrenNotExists";
import {AppError} from "src/shared/error/AppError";
import {ParentNotExists} from "./exception/ParentNotExists";
import {ParentEditDTO} from "./dto/edit.dto";
import {ParentsDTO} from "./dto/parent.dto";

@Injectable()
export class ParentService {
  constructor(
    private parentRepository: ParentsRepository,
    @Inject(forwardRef(() => BirdService))
    private birdService: BirdService,
  ) {}

  async findById(id: number) {
    const finded = await this.parentRepository.findByWithRelation({id}, [
      "father",
      "mother",
    ]);

    if (!finded) throw new ParentNotExists();

    return finded;
  }

  async create(parentToBeCreate: ParentsCreateDto) {
    const checkFather = await this.birdService.findById(
      parentToBeCreate.father,
    );

    if (!["Macho"].includes(checkFather.gender))
      throw new FatherGenderInválid();

    const checkMother = await this.birdService.findById(
      parentToBeCreate.mother,
    );

    if (!["Fêmea"].includes(checkMother.gender))
      throw new FatherGenderInválid();

    const parents = new Parent();
    parents.mother = checkMother;
    parents.father = checkFather;

    await this.parentRepository.store(parents);

    return parents;
  }

  async update(
    id_parent: number,
    parentsToUpdate: ParentEditDTO,
  ): Promise<any> {
    const checkParents = await this.parentRepository.findById(id_parent);

    if (!checkParents) throw new ParentNotExists();

    const checkMother = await this.birdService.findById(parentsToUpdate.mother);
    if (!["Fêmea"].includes(checkMother.gender))
      throw new FatherGenderInválid();

    const checkFather = await this.birdService.findById(parentsToUpdate.father);

    if (!["Macho"].includes(checkFather.gender))
      throw new FatherGenderInválid();

    const parentsCreate = this.parentRepository.create({
      id: checkParents.id,
      ...checkParents,
      mother: checkMother,
      father: checkFather,
    });

    await this.parentRepository.updateParents(parentsCreate);

    return parentsCreate;
  }

  async delete(id: number) {
    const parent = await this.parentRepository.findById(id);

    if (!parent) throw new ParentNotExists();

    this.parentRepository.destroy(parent);
  }
}
