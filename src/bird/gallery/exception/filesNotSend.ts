import {AppError} from "src/shared/error/AppError";

export class FilesNotSend extends AppError {
  constructor() {
    super("Arquivo inexistente, tente novamente", 404);
  }
}
