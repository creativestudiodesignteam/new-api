import {DeepPartial, EntityRepository, Repository} from "typeorm";
import {AllowedEggsRelation} from "./interface/allowedEggsRelations";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {AccountDTO} from "src/account/dto/account.dto";
import {Eggs} from "./egg.entity";
import { EggsCreateDto } from "./dto/create.dto";
/* import {EggsQueryParams} from "./dto/bird-query-params.dto"; */

@EntityRepository(Eggs)
export class EggsRepository extends Repository<Eggs> {
  async findAll(queryParams): Promise<Eggs[]> {
    const eggs = this.find({
      take: queryParams.perPage,
      skip: queryParams.perPage * (queryParams.page - 1),
    });

    return eggs;
  }

  findById(id: number): Promise<Eggs> {
    return this.findOne(id);
  }

  async findBy(paramToSearch) {
    return this.findOne({where: paramToSearch});
  }

  async findByWithRelation(paramToSearch, relation: AllowedEggsRelation[]) {
    return this.findOne({where: paramToSearch, relations: relation});
  }

  async findAllByWithRelation(
    paramToSearch,
    relation: AllowedEggsRelation[],
    queryParams: QueryParams,
  ) {
    return await this.find({
      where: paramToSearch,
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      relations: relation,
    });
  }

  async findCountAll(account: AccountDTO): Promise<number> {
    return await this.count({
      where: {account},
    });
  }

  //Todo adicionar o DTO de criação
  store(birdToBeCreated: EggsCreateDto) {
    return this.insert(birdToBeCreated);
  }

  //Todo adicionar o DTO do deepPartial
  async updateEggs(eggs: DeepPartial<any>) {
    return await this.update(eggs.id, eggs);
  }

  //Todo adicionar o DTO
  destroy(eggs: Eggs) {
    this.remove(eggs);
  }
}
