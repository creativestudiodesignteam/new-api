import {AppError} from "src/shared/error/AppError";

export class CategoriesNotExists extends AppError {
  constructor() {
    super("Categories inexisteste", 404);
  }
}
