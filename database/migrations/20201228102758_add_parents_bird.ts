import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.table("birds", table => {
    table
      .integer("parents")
      .references("id")
      .inTable("birds_parents")
      .onDelete("SET NULL")
      .onUpdate("CASCADE");
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.table("birds", function(table) {
    table.dropColumn("parents");
  });
}
