import {AppError} from "src/shared/error/AppError";

export class FatherGenderInválid extends AppError {
  constructor() {
    super("Este passáro não pode ser o mãe", 400);
  }
}
