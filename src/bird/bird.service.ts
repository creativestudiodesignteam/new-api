import {forwardRef, Inject, Injectable} from "@nestjs/common";
import {AccountService} from "src/account/account.service";
import {AccountDTO} from "src/account/dto/account.dto";
import {BreedService} from "src/breed/breed.service";
import {BreedsColorsService} from "src/breed/colors/colors.service";
import {CageService} from "src/cage/cage.service";
import {CategoriesService} from "src/categories/categories.service";
import {ColorsService} from "src/colors/colors.service";
import {StatusService} from "src/status/status.service";
import {BirdsRepository} from "./bird.repository";
import {BirdsCreateDtoIn} from "./dto/create-in.dto";
import {BirdsCreateDtoOut} from "./dto/create-out.dto";
import {QueryParamsWithFilter} from "./dto/queryParamsWithFilter.dto";
import {SearchParams} from "./dto/search.dto";
import {BirdsNotExists} from "./exception/BirdNotExists";
import {BreedsAndColorNotExists} from "./exception/BreedsAndColorNotExists";
import {GenderInvalid} from "./exception/genderVálid";
import {ParentsCreateDto} from "./parent/dto/create.dto";
import {Parent} from "./parent/parent.entity";
import {ParentService} from "./parent/parent.service";

@Injectable()
export class BirdService {
  constructor(
    private birdsRepository: BirdsRepository,
    private categoriesService: CategoriesService,
    private breedService: BreedService,
    private colorsService: ColorsService,
    private breedsColorsService: BreedsColorsService,
    private statusService: StatusService,
    private accountService: AccountService,
    private cageService: CageService,

    @Inject(forwardRef(() => ParentService))
    private parentService: ParentService,
  ) {}

  async findById(id_bird: number) {
    const findedAllBirds = await this.birdsRepository.findByWithRelation(
      {
        id: id_bird,
      },
      ["status", "color", "breeds", "category", "cage"],
    );

    if (!findedAllBirds) throw new BirdsNotExists();

    return findedAllBirds;
  }

  async findByIdAccount(id_account: number, id_bird: number) {
    const checkAccount = await this.accountService.findById(id_account);

    const findedAllBirds = await this.birdsRepository.findByWithRelation(
      {
        id: id_bird,
        account: checkAccount,
      },
      ["status", "color", "breeds", "category", "cage"],
    );

    if (!findedAllBirds) throw new BirdsNotExists();

    return findedAllBirds;
  }

  async findByIdNotRelation(id: number) {
    const findedAllBirds = await this.birdsRepository.findById(id);

    if (!findedAllBirds) throw new BirdsNotExists();

    return findedAllBirds;
  }
  async findCountByAccount(id_account: number) {
    const checkAccount = await this.accountService.findById(id_account);

    const findedAllBirds = await this.birdsRepository.findCountAll(
      checkAccount,
    );

    return findedAllBirds;
  }

  async readAll(queryParams: QueryParamsWithFilter) {
    const findedAllBirds = await this.birdsRepository.findAllByWithRelation(
      {},
      ["status", "color", "breeds", "category", "cage"],
      queryParams,
    );

    return findedAllBirds;
  }

  async readAllByLogger(
    id_account: number,
    queryParams: QueryParamsWithFilter,
  ) {
    const checkAccount = await this.accountService.findById(id_account);

    var searchCryteria: SearchParams = {};
    if (checkAccount) searchCryteria.account = checkAccount;
    if (queryParams.gender) searchCryteria.gender = queryParams.gender;
    if (queryParams.color) searchCryteria.color = queryParams.color;
    if (queryParams.category) searchCryteria.category = queryParams.category;
    if (queryParams.breeds) searchCryteria.breeds = queryParams.breeds;
    if (queryParams.status) searchCryteria.status = queryParams.status;

    const findedAllBirds = await this.birdsRepository.findAllByWithRelation(
      searchCryteria,
      ["status", "color", "breeds", "category", "cage"],
      queryParams,
    );

    return findedAllBirds;
  }

  async create(
    account,
    birdsToBeCreated: BirdsCreateDtoIn,
  ): Promise<BirdsCreateDtoOut> {
    const checkAccount = await this.accountService.findById(account);

    const checkCategory = await this.categoriesService.findById(
      birdsToBeCreated.category,
    );

    const checkBreeds = await this.breedService.findByIdOnlyBreed(
      birdsToBeCreated.breeds,
    );

    const checkColor = await this.colorsService.findById(
      birdsToBeCreated.color,
    );

    const checkCages = await this.cageService.findById(
      birdsToBeCreated.cages,
      account,
    );

    const checkColorExistsInBreed = await this.breedsColorsService.findColorAndBreed(
      checkColor.id,
      checkBreeds.id,
    );

    if (checkColorExistsInBreed.length <= 0) {
      throw new BreedsAndColorNotExists();
    }
    const checkStatus = await this.statusService.findById(
      birdsToBeCreated.status,
    );

    if (!["Macho", "Fêmea"].includes(birdsToBeCreated.gender))
      throw new GenderInvalid();

    if (birdsToBeCreated.father && birdsToBeCreated.mother) {
      const parents = new ParentsCreateDto();
      parents.father = birdsToBeCreated.father;
      parents.mother = birdsToBeCreated.mother;

      var createdParents = await this.parentService.create(parents);
    }
    const birds = new BirdsCreateDtoOut();
    birds.name = birdsToBeCreated.name;
    birds.birthdate = birdsToBeCreated.birthdate;
    birds.rightRing = birdsToBeCreated.rightRing;
    birds.leftRing = birdsToBeCreated.leftRing;
    birds.gender = birdsToBeCreated.gender;
    birds.registry = birdsToBeCreated.registry;
    birds.category = checkCategory;
    birds.breeds = checkBreeds;
    birds.color = checkColor;
    birds.status = checkStatus;
    birds.cage = checkCages;
    birds.account = checkAccount;
    birds.parents = createdParents ?? null;

    await this.birdsRepository.store(birds);

    return birds;
  }

  async update(
    id_account: number,
    id_bird: number,
    birdToUpdate: BirdsCreateDtoIn,
  ) {
    const checkAccount = await this.accountService.findById(id_account);

    const checkBird = await this.birdsRepository.findBy({
      id: id_bird,
      account: checkAccount,
    });

    if (!checkBird) throw new BirdsNotExists();

    if (birdToUpdate.gender) {
      if (!["Macho", "Femea"].includes(birdToUpdate.gender))
        throw new GenderInvalid();
    }

    if (birdToUpdate.breeds) {
      var checkCategory = await this.categoriesService.findById(
        birdToUpdate.category,
      );
    }
    if (birdToUpdate.breeds) {
      var checkBreeds = await this.breedService.findByIdOnlyBreed(
        birdToUpdate.breeds,
      );
    }

    if (birdToUpdate.breeds) {
      var checkColor = await this.colorsService.findById(birdToUpdate.color);
    }
    if (birdToUpdate.breeds) {
      var checkColorExistsInBreed = await this.breedsColorsService.findColorAndBreed(
        checkColor.id,
        checkBreeds.id,
      );
    }
    if (birdToUpdate.breeds) {
      if (checkColorExistsInBreed.length <= 0) {
        throw new BreedsAndColorNotExists();
      }
    }
    if (birdToUpdate.breeds) {
      var checkStatus = await this.statusService.findById(birdToUpdate.status);
    }

    const birds = this.birdsRepository.create({
      id: checkBird.id,
      ...checkBird,
      name: birdToUpdate.name,
      birthdate: birdToUpdate.birthdate,
      rightRing: birdToUpdate.rightRing,
      leftRing: birdToUpdate.leftRing,
      gender: birdToUpdate.gender,
      registry: birdToUpdate.registry,
      breeds: checkBreeds,
      category: checkCategory,
      color: checkColor,
      status: checkStatus,
    });

    await this.birdsRepository.updateBirds(birds);

    return birds;
  }

  async updateParents(id_account: number, id_bird: number, parents: Parent) {
    const checkAccount = await this.accountService.findById(id_account);

    const checkBird = await this.birdsRepository.findBy({
      id: id_bird,
      account: checkAccount,
    });

    if (!checkBird) throw new BirdsNotExists();

    const birds = this.birdsRepository.create({
      id: checkBird.id,
      ...checkBird,
      parents,
    });

    await this.birdsRepository.updateBirds(birds);

    return birds;
  }

  async delete(id: number) {
    const birds = await this.birdsRepository.findById(id);

    if (!birds) throw new BirdsNotExists();
    this.birdsRepository.destroy(birds);
  }
}
