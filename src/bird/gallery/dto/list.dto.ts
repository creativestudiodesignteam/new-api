import {ImagesListDto} from "src/image/dto/list.dto";
import {GalleryDTO} from "./gallery.dto";

export class GalleryListDto {
  galeries: GalleryDTO[];
  page: number;
  perPage: number;

  constructor(galeries: GalleryDTO[], page: number, perPage: number) {
    galeries.map(result => {
      result.image.url = `${process.env.URL_SERVER}/files/read/${result.image.name}`;

      return result;
    });

    this.galeries = galeries;
    this.page = page;
    this.perPage = perPage;
  }

  public getGaleryData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      Galery: this.galeries,
    };
  }
}
