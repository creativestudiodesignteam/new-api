import {IsDate, IsNumber, IsNumberString, IsOptional} from "class-validator";
import {AccountDTO} from "src/account/dto/account.dto";
import {ParentsDTO} from "src/bird/parent/dto/parent.dto";
import {CageDTO} from "src/cage/dto/cage.dto";
import {StatusDTO} from "src/status/dto/status.dto";
export class BabiesOutCreateDto {
  id: number;
  birthdate: Date;
  cage: CageDTO;
  status: StatusDTO;
  parents: ParentsDTO;
  account: AccountDTO;
}
