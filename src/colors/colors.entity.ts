import {Birds} from "src/bird/bird.entity";
import {BreedsColors} from "src/breed/colors/colors.entity";
import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";

@Entity({name: "colors"})
export class Colors {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(
    () => BreedsColors,
    breedsColors => breedsColors.breed_color,
  )
  breedsColors: BreedsColors;

  @OneToMany(
    () => Birds,
    birds => birds.color,
  )
  birds: Birds;
}
