import {AppError} from "src/shared/error/AppError";

export class FilenameExists extends AppError {
  constructor() {
    super("Nome do arquivo já existe", 400);
  }
}
