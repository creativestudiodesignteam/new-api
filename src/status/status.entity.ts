import {Babies} from "src/bird/baby/baby.entity";
import {Birds} from "src/bird/bird.entity";
import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";

@Entity({name: "status"})
export class Status {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(
    () => Birds,
    birds => birds.status,
  )
  birds: Birds;

  @OneToMany(
    () => Babies,
    babies => babies.status,
  )
  babies: Babies;
}
