import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("birds_babies", table => {
    table.increments("id").primary();
    table.date("birthdate").notNullable();
    table.string("rightRing", 80);
    table.string("leftRing", 80);
    table.date("ring_date");
    table.date("separete_date");
    table
      .integer("account")
      .references("id")
      .inTable("accounts")
      .notNullable()
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    table
      .integer("cages")
      .references("id")
      .inTable("cages")
      .notNullable()
      .onDelete("SET NULL")
      .onUpdate("CASCADE");
    table
      .integer("status")
      .references("id")
      .inTable("status")
      .notNullable()
      .onDelete("CASCADE")
      .onUpdate("CASCADE")
      .defaultTo(1);
    table
      .integer("parents")
      .references("id")
      .inTable("birds_parents")
      .onDelete("SET NULL")
      .onUpdate("CASCADE");

    table.timestamps(undefined, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("birds_babies");
}
