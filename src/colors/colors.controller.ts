import {ValidationPipe} from "../shared/pipes/validation.pipe";
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Query,
  Post,
  Request,
  Put,
} from "@nestjs/common";
import {ColorsCreateDto} from "./dto/create.dto";

// import {ApiTags} from "@nestjs/swagger";

import {ColorsListDto} from "./dto/list.dto";
import {CustomSuccessResponse} from "src/shared/dto/customResponse.dto";
import {ColorsService} from "./colors.service";
import {ColorsDTO} from "./dto/colors.dto";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {ColorsEditDto} from "./dto/edit.dto";

// @ApiTags("Colors")
@Controller("colors")
export class ColorsController {
  constructor(private colorsService: ColorsService) {}

  @Get()
  async findAll(
    @Query(new ValidationPipe()) queryParams: QueryParams,
  ): Promise<any> {
    const ColorsFindAll = await this.colorsService.findAll(queryParams);

    const paginatedList = new ColorsListDto(
      ColorsFindAll,
      queryParams.page ?? 1,
      queryParams.perPage ?? 10,
    );

    return paginatedList;
  }

  @Get(":id")
  async findById(@Param() param): Promise<ColorsDTO> {
    const ColorsFindById = await this.colorsService.findById(param.id);
    return ColorsFindById;
  }

  @Post()
  async create(@Body(new ValidationPipe()) colors: ColorsCreateDto) {
    const ColorsCreate = await this.colorsService.create(colors);
    return ColorsCreate;
  }

  @Put()
  async update(
    @Request() req,
    @Body(new ValidationPipe())
    colorsEdit: ColorsEditDto,
  ): Promise<ColorsDTO> {
    const updateColors = this.colorsService.update(req.id, colorsEdit);

    return updateColors;
  }

  @Delete()
  @HttpCode(200)
  async delete(@Param() param): Promise<CustomSuccessResponse> {
    await this.colorsService.delete(param.id);

    return new CustomSuccessResponse("Colors excluido com sucesso");
  }
}
