import {IsNumber, IsString} from "class-validator";
import {BirdsDTO} from "src/bird/dto/birds.dto";

export class ParentEditDTO {
  mother: number;
  father: number;
}
