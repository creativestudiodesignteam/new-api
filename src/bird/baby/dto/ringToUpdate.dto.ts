import {IsDate, IsOptional, IsString} from "class-validator";
export class BabiesRingDto {
  id: number;

  @IsString()
  rightRing: string;

  @IsString()
  leftRing: string;

  @IsOptional()
  @IsDate()
  ring_date: Date;
}
