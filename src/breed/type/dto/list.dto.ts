import {BreedsTypesDTO} from "./types.dto";

export class TypesListDto {
  Types: BreedsTypesDTO[];
  page: number;
  perPage: number;

  constructor(types: BreedsTypesDTO[], page: number, perPage: number) {
    this.Types = types;
    this.page = page;
    this.perPage = perPage;
  }

  public getTypesData() {
    return {
      pagination: {
        page: this.page,
        perPage: this.perPage,
      },
      Types: this.Types,
    };
  }
}
