import {IsNumber} from "class-validator";
import {Birds} from "src/bird/bird.entity";
import {BirdsDTO} from "src/bird/dto/birds.dto";
import {BirdsToListDTO} from "src/bird/dto/birdsToList.dto";
import {ImageWithLink} from "src/image/dto/imageWithLink";
import {ImagesListDto} from "src/image/dto/list.dto";

export class GalleryCreateDtoIn {
  @IsNumber()
  id: number;
  image: ImagesListDto;
  bird: Birds;
}
