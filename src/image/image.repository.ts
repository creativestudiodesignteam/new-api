import {Images} from "./image.entity";
import {DeepPartial, EntityRepository, Repository} from "typeorm";
/* import {ImagesQueryParams} from "./dto/image-query-params.dto"; */

@EntityRepository(Images)
export class ImagesRepository extends Repository<Images> {
  async findAll(queryParams): Promise<Images[]> {
    const images = this.find({
      take: queryParams.perPage,
      skip: queryParams.perPage * (queryParams.page - 1),
    });

    return images;
  }

  findById(id: number): Promise<Images> {
    return this.findOne(id);
  }

  async findBy(paramToSearch) {
    return this.findOne({where: paramToSearch});
  }

  store(imageToBeCreated: Images) {
    return this.insert(imageToBeCreated);
  }

  async updateImages(image: DeepPartial<Images>) {
    return await this.update(image.id, image);
  }

  destroy(image: Images) {
    this.remove(image);
  }
}
