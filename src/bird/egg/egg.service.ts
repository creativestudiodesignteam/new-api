import {Injectable} from "@nestjs/common";
import {add} from "date-fns";
import {now} from "moment";
import {AccountService} from "src/account/account.service";
import {CageService} from "src/cage/cage.service";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {AppError} from "src/shared/error/AppError";
import {BabyService} from "../baby/baby.service";
import {BabiesDTO} from "../baby/dto/babies.dto";
import {BabiesCreateDto} from "../baby/dto/create.dto";
import {ParentsCreateDto} from "../parent/dto/create.dto";
import {ParentService} from "../parent/parent.service";
import {EggsCreateDto} from "./dto/create.dto";
import {EggsDTO} from "./dto/egg.dto";
import {IncubateDTO} from "./dto/incubate.dto";
import {EggsRepository} from "./egg.repository";
import {EggHasAlreadyBeenHatched} from "./exception/EggHasAlreadyBeenHatched";
import {EggsNotExists} from "./exception/EggsNotExists";

@Injectable()
export class EggService {
  constructor(
    private eggsRepository: EggsRepository,
    private accountService: AccountService,
    private cageService: CageService,
    private parentService: ParentService,
    private babyService: BabyService,
  ) {}

  async hatch(account: number, id: number) {
    const checkEgg = await this.eggsRepository.findByWithRelation(
      {
        id,
        account,
      },
      ["cage", "parents", "account"],
    );

    if (!checkEgg) throw new EggsNotExists();

    if (checkEgg.isShocked) throw new EggHasAlreadyBeenHatched();

    const eggsToIncubated = this.eggsRepository.create({
      id: checkEgg.id,
      ...checkEgg,
      birthdate: new Date(now()),
      state: "Nascido",
      isShocked: true,
    });

    await this.eggsRepository.updateEggs(eggsToIncubated);

    const babyToTransform = new BabiesCreateDto();
    babyToTransform.mother = checkEgg.parents.mother.id;
    babyToTransform.father = checkEgg.parents.father.id;
    babyToTransform.birthdate = eggsToIncubated.birthdate;
    babyToTransform.cage = eggsToIncubated.cage.id;

    const createToBaby = await this.babyService.create(
      account,
      babyToTransform,
    );

    return createToBaby;
  }

  async fertilize(account: number, id: number) {
    const checkEgg = await this.eggsRepository.findByWithRelation(
      {
        id,
        account,
      },
      ["cage", "parents", "account"],
    );

    if (!checkEgg) throw new EggsNotExists();

    if (checkEgg.isShocked) throw new EggHasAlreadyBeenHatched();

    const eggsToIncubated = this.eggsRepository.create({
      id: checkEgg.id,
      ...checkEgg,
      state: "Nasceu",
    });

    await this.eggsRepository.updateEggs(eggsToIncubated);

    delete eggsToIncubated.account.password;

    return eggsToIncubated;
  }

  async incubate(account: number, id: number, incubateToUpdate: IncubateDTO) {
    const checkEgg = await this.eggsRepository.findByWithRelation(
      {
        id,
        account,
      },
      ["cage", "parents", "account"],
    );

    if (!checkEgg) throw new EggsNotExists();

    if (checkEgg.isShocked) throw new EggHasAlreadyBeenHatched();

    if (checkEgg.candlingDate || checkEgg.hatchingDate)
      throw new AppError("Este ovo já foi incubado", 400);

    const dateNow = new Date(now());

    const eggsToIncubated = this.eggsRepository.create({
      id: checkEgg.id,
      ...checkEgg,
      birthdate: add(new Date(now()), {
        days: incubateToUpdate.birthdate,
      }),
      candlingDate: add(new Date(now()), {
        days: incubateToUpdate.cadlingDate,
      }),
      hatchingDate: dateNow,
      state: "Fertilizar",
    });

    await this.eggsRepository.updateEggs(eggsToIncubated);

    delete eggsToIncubated.account.password;

    return eggsToIncubated;
  }

  async findByCage(cage: number, queryParams: QueryParams) {
    const findAll = await this.eggsRepository.findAllByWithRelation(
      {cage},
      ["account", "cage", "parents"],
      queryParams,
    );

    return findAll;
  }

  async findAll(account: number, queryParams: QueryParams) {
    const findAll = await this.eggsRepository.findAllByWithRelation(
      {account, isShocked: false},
      ["account", "cage", "parents"],
      queryParams,
    );

    return findAll;
  }

  async addParents(id: number, parentsToAdd) {
    const checkEgg = await this.eggsRepository.findByWithRelation({id}, [
      "parents",
      "account",
      "cage",
    ]);
    if (!checkEgg) throw new EggsNotExists();

    const parents = new ParentsCreateDto();
    parents.father = parentsToAdd.father;
    parents.mother = parentsToAdd.mother;

    var createdParents = await this.parentService.create(parents);

    const eggsWithParents = this.eggsRepository.create({
      id: checkEgg.id,
      ...checkEgg,
      parents: createdParents,
    });

    await this.eggsRepository.updateEggs(eggsWithParents);

    delete eggsWithParents.account.password;

    return eggsWithParents;
  }

  async findById(eggs: number) {
    const checkEggs = await this.eggsRepository.findByWithRelation(
      {
        id: eggs,
      },
      ["account", "cage", "parents"],
    );

    if (!checkEggs) throw new EggsNotExists();

    delete checkEggs.account.password;

    return new EggsDTO(
      checkEggs.id,
      checkEggs.birthdate,
      checkEggs.layingDate,
      checkEggs.hatchingDate,
      checkEggs.candlingDate,
      checkEggs.isShocked,
      checkEggs.state,
      checkEggs.account,
      checkEggs.cage,
      checkEggs.parents,
    );
  }

  async create(account: number, cage: number) {
    const checkAccount = await this.accountService.findById(account);

    const checkCage = await this.cageService.findById(cage, account);

    const dateNow = new Date(now());

    const eggs = new EggsCreateDto();
    eggs.layingDate = dateNow;
    eggs.state = "Chocar";
    eggs.cage = checkCage;
    eggs.account = checkAccount;

    await this.eggsRepository.store(eggs);

    return eggs;
  }

  async delete(id: number) {
    const eggs = await this.eggsRepository.findById(id);

    if (!eggs) throw new EggsNotExists();

    this.eggsRepository.destroy(eggs);
  }
}
