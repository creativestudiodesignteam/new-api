import {Account} from "src/account/account.entity";
import {Breeds} from "src/breed/breed.entity";
import {Categories} from "src/categories/categories.entity";
import {Colors} from "src/colors/colors.entity";
import {Status} from "src/status/status.entity";
import {Cage} from "src/cage/cage.entity";

import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import {Birds} from "../bird.entity";
import {Babies} from "../baby/baby.entity";

@Entity({name: "birds_parents"})
export class Parent {
  @PrimaryGeneratedColumn()
  id: number;
  @ManyToOne(
    () => Birds,
    birds => birds.mother,
    {
      eager: true,
    },
  )
  @JoinColumn({name: "mother"})
  mother: Birds;

  @ManyToOne(
    () => Birds,
    birds => birds.father,
    {
      eager: true,
    },
  )
  @JoinColumn({name: "father"})
  father: Birds;

  @OneToMany(
    () => Birds,
    birds => birds.parents,
  )
  birds: Parent;

  @OneToMany(
    () => Babies,
    babies => babies.parents,
  )
  babies: Babies;
}
