import {
  Body,
  Controller,
  Post,
  UseGuards,
  Request,
  Param,
  Delete,
  HttpCode,
  Get,
  Put,
} from "@nestjs/common";
import {JwtAuthGuard} from "src/auth/jwt/jwt-auth.guard";
import {CustomSuccessResponse} from "src/shared/dto/customResponse.dto";
import {ValidationPipe} from "src/shared/pipes/validation.pipe";
import {ParentsCreateDto} from "./dto/create.dto";
import {ParentEditDTO} from "./dto/edit.dto";
import {ParentService} from "./parent.service";

@Controller("parent")
export class ParentController {
  constructor(private parentService: ParentService) {}

  @UseGuards(JwtAuthGuard)
  @Get("read/:id")
  async findById(@Request() req, @Param() param) {
    const findBird = await this.parentService.findById(param.id);

    return findBird;
  }

  @UseGuards(JwtAuthGuard)
  @Post(":children")
  async create(
    @Request() req,
    @Param() param,
    @Body(new ValidationPipe())
    parentsToCreate: ParentsCreateDto,
  ) {
    const createdParents = await this.parentService.create(parentsToCreate);

    return createdParents;
  }

  @UseGuards(JwtAuthGuard)
  @Put(":id")
  async update(
    @Param() param,
    @Body(new ValidationPipe())
    parentsToUpdate: ParentEditDTO,
  ) {
    const parentsUpdated = await this.parentService.update(
      param.id,
      parentsToUpdate,
    );

    return parentsUpdated;
  }

  @Delete(":id")
  @HttpCode(200)
  async delete(@Param() param): Promise<CustomSuccessResponse> {
    await this.parentService.delete(param.id);

    return new CustomSuccessResponse("Parentes excluidos com sucesso");
  }
}
