import {AccountDTO} from "src/account/dto/account.dto";
import {QueryParams} from "src/shared/dto/queryParams.dto";
import {DeepPartial, EntityRepository, Repository} from "typeorm";
import {Cage} from "./cage.entity";
import { CageDTO } from "./dto/cage.dto";
import {AllowedCagesRelation} from "./interface/AllowedCagesRelations";

@EntityRepository(Cage)
export class CagesRepository extends Repository<Cage> {
  async findAll(queryParams: QueryParams): Promise<Cage[]> {
    const cages = await this.find({
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      order: {name: "ASC"},
    });

    return cages;
  }

  async findAllByUser(
    account: AccountDTO,
    queryParams: QueryParams,
  ): Promise<Cage[]> {
    const cages = await this.find({
      take: queryParams.perPage ? queryParams.perPage : 10,
      skip:
        queryParams.page && queryParams.perPage
          ? queryParams.perPage * (queryParams.page - 1)
          : 0,
      order: {name: "ASC"},
      where: {account},
    });

    return cages;
  }

  async findAllByUserWithoutPagination(
    account: AccountDTO,
  ): Promise<Cage[]> {
    const cages = await this.find({
      order: {name: "ASC"},
      where: {account},
    });

    return cages;
  }

  async findById(id: number): Promise<Cage> {
    return await this.findOne(id);
  }

  async findWithRelation(id: number, relation: AllowedCagesRelation[]) {
    return await this.findOne({relations: relation, where: {id}});
  }

  async findBy(paramToSearch) {
    return await this.findOne({where: paramToSearch});
  }

  async store(cagesToBeCreated): Promise<any> {
    return await this.insert(cagesToBeCreated);
  }

  async updateCages(cages: DeepPartial<CageDTO>) {
    return await this.update(cages.id, cages);
  }

  async destroy(cages): Promise<void> {
    await this.remove(cages);
  }
}
